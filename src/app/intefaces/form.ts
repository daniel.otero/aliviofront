export interface Form {
    category: String;
    subCategory: String;
    costProduct: Number;
    codeAffiliate: any;
    nameAffiliate: any;
    contactPerson: String;
    phoneContact: String;
    typeRate: String;
    rate: any;
    typeService: String;
    destinationService: any;
    numberPay: Number;
    typeDocument: String;
    numberDocument: number;
    firstName: String;
    secondName: String;
    lastName: String;
    secondLastName: String;
    date: any;
    deparmentBirth: String;
    cityBirth: String;
    gender: String;
    dependents: String;
    numberSon: Number;
    typeLiving: String;
    state: String;
    adress: String;
    phoneLiving: String;
    expirationDate: Number;
    contryLiving: String;
    levelStudies: String;
    phone:Number;
    email: String;
    deparmetn: String;
    city: String;
    neighborhood: String;
    socialStratum: any;
    timeliving: Number;
    typeActivity: any;
    typeContract: any;
    company: String;
    deparmentCompany: String;
    cityCompany: String;
    timeCompany: String;
    position: String;
    eps: String;
    pension: String;
    datePension: any;
    adressCompany: any;
    phoneCompany: Number;
    salary:  Number;
    fee: Number;
    commissions: Number;
    otherSalary: Number;
    totalSalary: Number;
    expeneses: Number;
    totalExpenses: Number;
    valueRequested: Number;
    place: String;
    limit: any;
    seller: any;
    nameFamily: String;
    phoneFamily: any;
    similarFamily: String;
    deparmentFamily: String;
    cityFamily: String;
    namePersonal: String;
    phonePersonal: any;
    deparmentPersonal: String;
    cityPersonal: String;













}
