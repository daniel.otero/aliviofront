import { Entity } from './entity';
export interface Entitys {
    _id?: String;
    __v?: any;
    first: Entity;
    second: Entity;
    third: Entity;
    fourth: Entity;
    fifth: Entity;

}
