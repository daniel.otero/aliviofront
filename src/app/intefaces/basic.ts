export interface Basic {
    _id?: String;
    __v?: any;
    tipoId: string;
    primerNombre: string ;
    segundoNombre: string;
    primerApellido: string;
    segundoApellido: string;
    fechaNacimiento: Date;
    fechaExpedicion: Date;
    telefono: any;
    celular: any;
    email: string;
    genero: string;
    dependientes: string;
    hijos: Number;
    pais: String;
    nivelEstudios: String;
    ciudad: string;
    departamento: string;
    barrio: string;
    vivienda: string;
    estrato: string;
    tiempoVivienda: string;
    terminosCondiciones: boolean;

}
    