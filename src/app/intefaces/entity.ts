export interface Entity {
    _id?: String;
    __v?: any;
    entity: String;
    quotas: Number;
    cost: Number;
    place: Number;
}