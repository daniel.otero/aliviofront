export interface User  {
    _id?: String;
    __v?: any;
    name: String;
    lastName: String;
    user: String;
    clinic: String;
    type: String;
    email: String;
    password: String;
}
