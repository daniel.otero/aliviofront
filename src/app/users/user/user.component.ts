import { Component, OnInit } from '@angular/core';
import { User } from '../../intefaces/user.interface';
import { UsersService } from '../../services/users.service';
import { Router, ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  users: User = {
    name: '',
    lastName: '',
    user: '',
    clinic : '',
    type: '',
    email: '',
    password: '',
  };
  types: string[] = ['admin', 'user'];
  clinics  : string[] = ['clinca 1', 'clinica 2'];
  constructor(private userService:UsersService, private router:Router) { }

  ngOnInit() {
  }

  save() {
    console.log(this.users);
    this.userService.newUser(this.users).subscribe(data =>{
      if(typeof data !== 'string'){
        swal({
          title: "Guardado",
          text: "",
          buttonsStyling: false,
          confirmButtonClass: "btn btn-success",
          type: "success",
          onAfterClose: () => this.router.navigate(['/Users'])
        }).catch(swal.noop)
      }
    }, error => {
      console.log(error);
    });
  }

}
