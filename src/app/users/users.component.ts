import { Component, OnInit,AfterViewInit } from '@angular/core';
import { UsersService } from '../services/users.service';


declare interface DataTable {
  headerRow: string[];
  
  dataRows: string[][];
}
declare const $: any;
@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})

export class UsersComponent implements OnInit, AfterViewInit {

  public dataTable: DataTable;
  users:any[] = [];
  constructor(private userServices: UsersService) {
    this.userServices.getUsers().subscribe(data =>{
      console.log(data);
      this.users = data.body;
      console.log(this.users);
      this.dataTable = {
        headerRow :['Nombre', 'Usuario' , 'Correo', 'Tipo'],
        dataRows: [this.users],
      };
    });
   

   }

  ngOnInit() {
  }

  ngAfterViewInit() {
    $('#datatables').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Buscar usuario",
      },
      

    });
    const table = $('#datatables').DataTable();

     // Edit record
      table.on('click', '.edit', function(e) {
        const $tr = $(this).closest('tr');
        const data = table.row($tr).data();
        alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
        e.preventDefault();
      });

      // Delete a record
      table.on('click', '.remove', function(e) {
        const $tr = $(this).closest('tr');
        table.row($tr).remove().draw();
        e.preventDefault();
      });

      //Like record
      table.on('click', '.like', function(e) {
        alert('You clicked on Like button');
        e.preventDefault();
      });

      $('.card .material-datatables label').addClass('form-group');
      

    }

}
