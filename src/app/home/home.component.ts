import { Component, OnInit,ElementRef, OnDestroy } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit , OnDestroy {

  test: Date = new Date();
  private toggleButton: any;
  private sidebarVisible: boolean;
  private nativeElement: Node;

  user: String = '';
  pasw: String = ''; 

  constructor(private element: ElementRef, private router:Router) {
      this.nativeElement = element.nativeElement;
      this.sidebarVisible = false;
  }

  validate() {
    console.log(this.user)
    if (this.user === 'admin' && this.pasw === 'admin') {
      console.log('login succes');
      swal({
        title: "Bienvenido, " +'admin' ,
        text: "",
        buttonsStyling: false,
        confirmButtonClass: "btn btn-success",
        type: "success",
        onAfterClose: () => this.router.navigate(['/Form'])
    }).catch(swal.noop)
    }

  }

  ngOnInit() {
      var navbar : HTMLElement = this.element.nativeElement;
      this.toggleButton = navbar.getElementsByClassName('navbar-toggle')[0];
      const body = document.getElementsByTagName('body')[0];
      body.classList.add('login-page');
      body.classList.add('off-canvas-sidebar');
      const card = document.getElementsByClassName('card')[0];
      setTimeout(function() {
          // after 1000 ms we add the class animated to the login/register card
          card.classList.remove('card-hidden');
      }, 700);
  }
  sidebarToggle() {
      var toggleButton = this.toggleButton;
      var body = document.getElementsByTagName('body')[0];
      var sidebar = document.getElementsByClassName('navbar-collapse')[0];
      if (this.sidebarVisible == false) {
          setTimeout(function() {
              toggleButton.classList.add('toggled');
          }, 500);
          body.classList.add('nav-open');
          this.sidebarVisible = true;
      } else {
          this.toggleButton.classList.remove('toggled');
          this.sidebarVisible = false;
          body.classList.remove('nav-open');
      }
  }
  ngOnDestroy(){
    const body = document.getElementsByTagName('body')[0];
    body.classList.remove('login-page');
    body.classList.remove('off-canvas-sidebar');
  }

}
