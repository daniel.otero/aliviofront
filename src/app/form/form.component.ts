import { Component, OnInit } from '@angular/core';
import { Form } from '../intefaces/form';
import swal from 'sweetalert2';
import * as jsPDF from 'jspdf';
import * as html2canvas from 'html2canvas';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  state = 1;
  form: Form = {
    category: '',
    subCategory: '',
    costProduct: 0,
    codeAffiliate: '',
    nameAffiliate: '',
    contactPerson: '',
    phoneContact: '',
    typeRate: '',
    rate: '',
    typeService: '',
    destinationService: '',
    numberPay: 0,
    typeDocument: ' ',
    numberDocument: null,
    firstName: ' ',
    secondName: ' ',
    lastName: ' ',
    secondLastName: ' ',
    date: ' ',
    deparmentBirth: ' ',
    cityBirth: ' ',
    gender: ' ',
    dependents: ' ',
    numberSon: 0,
    typeLiving: ' ',
    state: ' ',
    adress: ' ',
    phoneLiving: ' ',
    expirationDate: 0,
    contryLiving: ' ',
    levelStudies: ' ',
    phone: 0,
    email: ' ',
    deparmetn: ' ',
    city: ' ',
    neighborhood: ' ',
    socialStratum: ' ',
    timeliving: 0,
    typeActivity: ' ',
    typeContract: ' ',
    company: ' ',
    deparmentCompany: ' ',
    cityCompany: ' ',
    timeCompany: ' ',
    position: ' ',
    eps: ' ',
    pension: ' ',
    datePension: ' ',
    adressCompany: ' ',
    phoneCompany: 0,
    salary:  0,
    fee: 0,
    commissions: 0,
    otherSalary: 0,
    totalSalary: 0,
    expeneses: 0,
    totalExpenses: 0,
    valueRequested: 0,
    place: ' ',
    limit: '',
    seller: ' ',
    nameFamily: ' ',
    phoneFamily: ' ',
    similarFamily: ' ',
    deparmentFamily: ' ',
    cityFamily: ' ',
    namePersonal: ' ',
    phonePersonal: ' ',
    deparmentPersonal: ' ',
    cityPersonal: ' ',
  };

  constructor() { }

  ngOnInit() {
  }
  changeState() {
    if ( this.state === 1 ) {
      this.state = 2;
      return true;
    }
    if (this.state === 2) {
      this.state = 3;
      return true;
    } else {
      this.state = 1;
      return  true;
    }

  }
  prevState() {
    this.state -= 1;
  }

  generarPDF(){
    html2canvas(document.getElementById('contenido'), {
      // Opciones
      allowTaint: true,
      useCORS: false,
      // Calidad del PDF
      scale: 1
    }).then(function(canvas) {
      let img = canvas.toDataURL('image/png');
      let doc = new jsPDF();
      doc.addImage(img,'PNG',7, 20, 195, 605);
      doc.save('Encuesta.pdf');
    });
  }

  save() {
    if ( this.form.numberDocument === 11111) {
      swal({
        title: "Felicidades su credito fue aprobado",
        text: "",
        buttonsStyling: false,
        confirmButtonClass: "btn btn-success",
        type: "success",
        
      }).catch(swal.noop);

    }

    if ( this.form.numberDocument === 22222) {
      swal({
        title: "Pruebe con otro método de pago",
        text: "",
        buttonsStyling: false,
        confirmButtonClass: "btn btn-warning",
        type: "warning",
        
      }).catch(swal.noop);

    }




  }
}
