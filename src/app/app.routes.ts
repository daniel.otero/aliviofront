import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { FormComponent} from './form/form.component';
import { UsersComponent } from './users/users.component';
import { UserComponent } from './users/user/user.component';
import { OrderComponent } from './order/order.component';
import { ReportsComponent } from './reports/reports.component';
import { RequestComponent } from './request/request.component';
import { AssignmentComponent } from './request/assignment/assignment.component';

const routes: Routes = [
    { path: 'Home', component: HomeComponent } ,
    {path: 'Form', component: FormComponent },
    {path: 'Users', component: UsersComponent },
    {path: 'Request', component: RequestComponent },
    {path: 'Request/:id', component:AssignmentComponent },
    {path: 'User/:id', component: UserComponent },
    {path: 'Order', component: OrderComponent },
    {path: 'Reports', component: ReportsComponent },
    { path: '**', pathMatch: 'full', redirectTo: 'Home'}
];
export class FeatureRoutingModule {}
export const APP_ROUTING = RouterModule.forRoot(routes);
