import { Component, OnInit } from '@angular/core';
declare interface DataTable {
  headerRow: string[];
  
  dataRows: any[];
}
declare interface info {
  fecha:any,
  solicitud:any,
  identificacion:string,
  estado:string,
  valorT:any,
  valorS:any,
  financiera:String,

} 
declare const $: any;
@Component({
  selector: 'app-request',
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.css']
})
export class RequestComponent implements OnInit {
  public dataTable: DataTable;
  info1:info = {
    fecha:'2/05/20',
    solicitud:'sol',
    identificacion:'100000',
    estado:'activo',
    valorT:'2999',
    valorS:'30232',
    financiera: 'Denti salud'
  };
  info2:info = {
    fecha:'6/05/20',
    solicitud:'sol',
    identificacion:'340000',
    estado:'activo',
    valorT:'8989',
    valorS:'30232',
    financiera: 'Refinancia'
  };
  constructor() { 
    this.dataTable = {
      headerRow :['Fecha', 'Solicitud' , 'Identificación', 'Estado', 'Valor Tratamiento', 'Valor solicitado','Financiera','Detalles'],
      dataRows: [this.info1, this.info2 ],
    };
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    $('#datatables').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Buscar asignación",
      },
      

    });
    const table = $('#datatables').DataTable();

     // Edit record
      table.on('click', '.edit', function(e) {
        const $tr = $(this).closest('tr');
        const data = table.row($tr).data();
        alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
        e.preventDefault();
      });

      // Delete a record
      table.on('click', '.remove', function(e) {
        const $tr = $(this).closest('tr');
        table.row($tr).remove().draw();
        e.preventDefault();
      });

      //Like record
      table.on('click', '.like', function(e) {
        alert('You clicked on Like button');
        e.preventDefault();
      });

      $('.card .material-datatables label').addClass('form-group');
      

    }

}
