(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/app.component.html":
/*!**************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/app.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <router-outlet></router-outlet> -->\r\n\r\n<!-- <app-login></app-login> -->\r\n<!-- <app-home></app-home> -->\r\n<router-outlet></router-outlet>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/form/form.component.html":
/*!********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/form/form.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"sidebar\" data-color=\"danger\" data-background-color=\"white\">\r\n    <app-sidebar-cmp></app-sidebar-cmp>\r\n    <div class=\"sidebar-background\"></div>\r\n</div>\r\n\r\n<div class=\"main-panel\">\r\n    <app-navbar-cmp></app-navbar-cmp>\r\n    <div class=\"main-content\">\r\n        <div class=\"container-fluid\">\r\n            <div class=\"row\">\r\n\r\n                <div class=\"col-md-11\" id=\"contenido\">\r\n\r\n                    <form (ngSubmit)=\"generarPDF()\" #forms=\"ngForm\">\r\n                        <div>\r\n                            <div class=\"card\">\r\n                                <div class=\"card-header card-header-muted card-header-icon\">\r\n                                    <div class=\"card-icon\">\r\n                                        <i class=\"material-icons\">content_paste</i>\r\n                                    </div>\r\n                                    <h4 class=\"card-title\">Encuesta cliente</h4>\r\n\r\n                                </div>\r\n\r\n\r\n\r\n                                <div>\r\n                                    <div class=\"card-body\">\r\n\r\n\r\n                                        <br>\r\n                                        <h4 class=\"txtB text-center\"><b>Información personal</b></h4>\r\n                                        <hr>\r\n                                        <div class=\"row\">\r\n\r\n                                            <div class=\"col-md-4\">\r\n                                                <mat-form-field class=\"example-full-width\">\r\n                                                    <input matInput placeholder=\"Categoria\" name=\"category\" [(ngModel)]=\"form.category\" required>\r\n\r\n                                                </mat-form-field>\r\n                                            </div>\r\n                                            <div class=\"col-md-4\">\r\n                                                <mat-form-field class=\"example-full-width\">\r\n                                                    <input matInput placeholder=\"Sub categoria\" name=\"subCategory\" [(ngModel)]=\"form.subCategory\" required>\r\n\r\n                                                </mat-form-field>\r\n                                            </div>\r\n                                            <div class=\"col-md-4\">\r\n                                                <mat-form-field class=\"example-full-width\">\r\n                                                    <input matInput placeholder=\"Valor producto\" name=\"costProduct\" [(ngModel)]=\"form.costProduct\" required>\r\n\r\n                                                </mat-form-field>\r\n                                            </div>\r\n                                            <!-- <div class=\"col-md-3\">\r\n                                            \r\n                                        </div> -->\r\n                                        </div>\r\n                                        <hr>\r\n                                        <div class=\"row\">\r\n                                            <div class=\"col-md-4\">\r\n                                                <mat-form-field class=\"example-full-width\">\r\n                                                    <input matInput placeholder=\"Código afiliado\" name=\"category\" [(ngModel)]=\"form.codeAffiliate\" required>\r\n\r\n                                                </mat-form-field>\r\n                                            </div>\r\n\r\n                                            <div class=\"col-md-4\">\r\n                                                <mat-form-field class=\"example-full-width\">\r\n                                                    <input matInput placeholder=\"Nombre afiliado\" name=\"nameAffilate\" [(ngModel)]=\"form.nameAffiliate\" required>\r\n\r\n                                                </mat-form-field>\r\n                                            </div>\r\n                                            <div class=\"col-md-4\">\r\n                                                <mat-form-field class=\"example-full-width\">\r\n                                                    <input matInput placeholder=\"Contacto afiliado\" name=\"contactPerson\" [(ngModel)]=\"form.contactPerson\" required>\r\n\r\n                                                </mat-form-field>\r\n                                            </div>\r\n                                            <!-- <div class=\"col-md-4\">\r\n                                            \r\n                                        </div> -->\r\n                                            <!-- <div class=\"col-md-3\">\r\n                                           \r\n                                        </div> -->\r\n                                        </div>\r\n                                        <hr>\r\n                                        <div class=\"row\">\r\n                                            <div class=\"col-md-4\">\r\n                                                <mat-form-field class=\"example-full-width\">\r\n                                                    <input matInput placeholder=\"Teléfono afiliado\" name=\"costProduct\" [(ngModel)]=\"form.phoneContact\" required>\r\n\r\n                                                </mat-form-field>\r\n                                            </div>\r\n                                            <div class=\"col-md-4\">\r\n                                                <mat-form-field class=\"example-full-width\">\r\n                                                    <input matInput placeholder=\"Tipo de tasa\" name=\"typeRate\" [(ngModel)]=\"form.typeRate\" required>\r\n\r\n                                                </mat-form-field>\r\n                                            </div>\r\n                                            <div class=\"col-md-4\">\r\n                                                <mat-form-field class=\"example-full-width\">\r\n                                                    <input matInput placeholder=\"Tasa\" name=\"rate\" [(ngModel)]=\"form.rate\" required>\r\n\r\n                                                </mat-form-field>\r\n                                            </div>\r\n                                            <!-- <div class=\"col-md-3\">\r\n                                           \r\n                                        </div>\r\n                                        <div class=\"col-md-3\">\r\n                                            \r\n                                        </div>\r\n                                        <div class=\"col-md-3\">\r\n                                            \r\n                                        </div> -->\r\n                                        </div>\r\n                                        <hr>\r\n                                        <div class=\"row\">\r\n                                            <div class=\"col-md-4\">\r\n                                                <mat-form-field class=\"example-full-width\">\r\n                                                    <input matInput placeholder=\"Tipo bien/servicio\" name=\"typeService\" [(ngModel)]=\"form.typeService\" required>\r\n\r\n                                                </mat-form-field>\r\n                                            </div>\r\n                                            <div class=\"col-md-4\">\r\n                                                <mat-form-field class=\"example-full-width\">\r\n                                                    <input matInput placeholder=\"Destinación bien\" name=\"destinationService\" [(ngModel)]=\"form.destinationService\" required>\r\n\r\n                                                </mat-form-field>\r\n                                            </div>\r\n                                            <div class=\"col-md-4\">\r\n                                                <mat-form-field class=\"example-full-width\">\r\n                                                    <input matInput placeholder=\"Número de pagaré\" name=\"numberPay\" [(ngModel)]=\"form.numberPay\" required>\r\n\r\n                                                </mat-form-field>\r\n                                            </div>\r\n                                            <!--                                         \r\n                                        <div class=\"col-md-3\">\r\n                                            \r\n                                        </div>\r\n                                        <div class=\"col-md-3\">\r\n                                            \r\n                                        </div>\r\n                                        <div class=\"col-md-3\">\r\n                                            \r\n                                        </div>\r\n                                        <div class=\"col-md-3\">\r\n                                            \r\n                                        </div> -->\r\n                                        </div>\r\n                                        <hr>\r\n\r\n                                        <div class=\"row\">\r\n                                            <div class=\"col-md-4\">\r\n                                                <mat-form-field>\r\n                                                    <mat-select placeholder=\"Tipo documento\" name=\"typeDocument\" [(ngModel)]=\"form.typeDocument\">\r\n                                                        <mat-option>\r\n                                                            Cc\r\n                                                        </mat-option>\r\n                                                        <mat-option>\r\n                                                            Pasaporte\r\n                                                        </mat-option>\r\n                                                    </mat-select>\r\n                                                </mat-form-field>\r\n                                            </div>\r\n                                            <div class=\"col-md-4\">\r\n                                                <mat-form-field class=\"example-full-width\">\r\n                                                    <input matInput placeholder=\"Número documento\" type=\"number\" name=\"numberDocument\" [(ngModel)]=\"form.numberDocument\" required>\r\n\r\n                                                </mat-form-field>\r\n                                            </div>\r\n                                            <div class=\"col-md-4\">\r\n                                                <mat-form-field class=\"example-full-width\">\r\n                                                    <input matInput placeholder=\"Primer nombre\" name=\"firstName\" [(ngModel)]=\"form.firstName\" required>\r\n\r\n                                                </mat-form-field>\r\n                                            </div>\r\n\r\n                                        </div>\r\n                                        <hr>\r\n                                        <div class=\"row\">\r\n                                            <div class=\"col-md-4\">\r\n                                                <mat-form-field class=\"example-full-width\">\r\n                                                    <input matInput placeholder=\"Segundo nombre\" name=\"secondName\" [(ngModel)]=\"form.secondName\" required>\r\n\r\n                                                </mat-form-field>\r\n                                            </div>\r\n                                            <div class=\"col-md-4\">\r\n                                                <mat-form-field class=\"example-full-width\">\r\n                                                    <input matInput placeholder=\"Primer apellido\" name=\"lastName\" [(ngModel)]=\"form.lastName\" required>\r\n\r\n                                                </mat-form-field>\r\n                                            </div>\r\n                                            <div class=\"col-md-4\">\r\n                                                <mat-form-field class=\"example-full-width\">\r\n                                                    <input matInput placeholder=\"Segundo apellido\" name=\"secondLastName\" [(ngModel)]=\"form.secondLastName\" required>\r\n\r\n                                                </mat-form-field>\r\n                                            </div>\r\n                                        </div>\r\n                                        <hr>\r\n\r\n                                        <div class=\"row\">\r\n\r\n                                            <div class=\"col-md-4\">\r\n                                                <mat-form-field class=\"example-full-width\">\r\n                                                    <input matInput type=\"date\" name=\"date\" [(ngModel)]=\"form.date\" required>\r\n\r\n                                                </mat-form-field>\r\n                                            </div>\r\n                                            <div class=\"col-md-4\">\r\n                                                <mat-form-field class=\"example-full-width\">\r\n                                                    <input matInput placeholder=\"Departamento nacimiento\" name=\"date\" [(ngModel)]=\"form.deparmentBirth\" required>\r\n\r\n                                                </mat-form-field>\r\n                                            </div>\r\n                                            <div class=\"col-md-4\">\r\n                                                <mat-form-field class=\"example-full-width\">\r\n                                                    <input matInput placeholder=\"Ciudad de nacimiento\" name=\"cityBirth\" [(ngModel)]=\"form.cityBirth\" required>\r\n\r\n                                                </mat-form-field>\r\n                                            </div>\r\n                                        </div>\r\n                                        <hr>\r\n                                        <div class=\"row\">\r\n\r\n\r\n                                            <div class=\"col-md-4\">\r\n                                                <mat-form-field>\r\n                                                    <mat-select placeholder=\"Genero\" name=\"gender\" [(ngModel)]=\"form.gender\">\r\n                                                        <mat-option>\r\n                                                            Masculino\r\n                                                        </mat-option>\r\n                                                        <mat-option>\r\n                                                            Femenino\r\n                                                        </mat-option>\r\n                                                        <mat-option>\r\n                                                            Otro\r\n                                                        </mat-option>\r\n                                                    </mat-select>\r\n                                                </mat-form-field>\r\n                                            </div>\r\n                                            <div class=\"col-md-4\">\r\n                                                <mat-form-field class=\"example-full-width\">\r\n                                                    <input matInput placeholder=\"Personas acargo\" name=\"dependents\" [(ngModel)]=\"form.dependents\" required>\r\n\r\n                                                </mat-form-field>\r\n                                            </div>\r\n                                            <div class=\"col-md-4\">\r\n                                                <mat-form-field class=\"example-full-width\">\r\n                                                    <input matInput placeholder=\"Número de hijos\" type=\"number\" name=\"numberSon\" [(ngModel)]=\"form.numberSon\" required>\r\n\r\n                                                </mat-form-field>\r\n                                            </div>\r\n                                        </div>\r\n                                        <hr>\r\n                                        <div class=\"row\">\r\n\r\n                                            <div class=\"col-md-4\">\r\n                                                <mat-form-field class=\"example-full-width\">\r\n                                                    <input matInput placeholder=\"Tipo de vivienda\" name=\"typeLiving\" [(ngModel)]=\"form.typeLiving\" required>\r\n\r\n                                                </mat-form-field>\r\n                                            </div>\r\n                                            <div class=\"col-md-4\">\r\n                                                <mat-form-field class=\"example-full-width\">\r\n                                                    <input matInput placeholder=\"Estado Civil\" name=\"state\" [(ngModel)]=\"form.state\" required>\r\n\r\n                                                </mat-form-field>\r\n                                            </div>\r\n                                            <div class=\"col-md-4\">\r\n                                                <mat-form-field class=\"example-full-width\">\r\n                                                    <input matInput placeholder=\"Dirección\" name=\"adress\" [(ngModel)]=\"form.adress\" required>\r\n\r\n                                                </mat-form-field>\r\n                                            </div>\r\n                                            <!-- <div class=\"col-md-3\">\r\n                                            \r\n                                        </div> -->\r\n                                        </div>\r\n                                        <hr>\r\n\r\n                                        <div class=\"row\">\r\n                                            <div class=\"col-md-4\">\r\n                                                <mat-form-field class=\"example-full-width\">\r\n                                                    <input matInput placeholder=\"Teléfono de vivienda\" name=\"phoneLiving\" [(ngModel)]=\"form.phoneLiving\" required>\r\n\r\n                                                </mat-form-field>\r\n                                            </div>\r\n\r\n                                            <div class=\"col-md-4\">\r\n                                                <mat-form-field class=\"example-full-width\">\r\n                                                    <input matInput placeholder=\"Fecha expedición documento\" type=\"date\" name=\"expirationDate\" [(ngModel)]=\"form.expirationDate\" required>\r\n\r\n                                                </mat-form-field>\r\n                                            </div>\r\n                                            <div class=\"col-md-4\">\r\n                                                <mat-form-field class=\"example-full-width\">\r\n                                                    <input matInput placeholder=\"País de residencia\" name=\"contryLiving\" [(ngModel)]=\"form.contryLiving\" required>\r\n\r\n                                                </mat-form-field>\r\n                                            </div>\r\n                                            <!-- <div class=\"col-md-3\">\r\n                                            \r\n                                        </div>\r\n                                        <div class=\"col-md-3\">\r\n                                            \r\n                                        </div> -->\r\n                                        </div>\r\n                                        <hr>\r\n                                        <div class=\"row\">\r\n                                            <div class=\"col-md-4\">\r\n                                                <mat-form-field class=\"example-full-width\">\r\n                                                    <input matInput placeholder=\"Nivel de estudios\" name=\"levelStudies\" [(ngModel)]=\"form.levelStudies\" required>\r\n\r\n                                                </mat-form-field>\r\n                                            </div>\r\n                                            <div class=\"col-md-4\">\r\n                                                <mat-form-field class=\"example-full-width\">\r\n                                                    <input matInput placeholder=\"Celular\" name=\"phone\" [(ngModel)]=\"form.phone\" required>\r\n\r\n                                                </mat-form-field>\r\n                                            </div>\r\n                                            <div class=\"col-md-4\">\r\n                                                <mat-form-field class=\"example-full-width\">\r\n                                                    <input matInput placeholder=\"Correo\" name=\"email\" [(ngModel)]=\"form.email\" required>\r\n\r\n                                                </mat-form-field>\r\n                                            </div>\r\n\r\n                                        </div>\r\n                                        <hr>\r\n\r\n                                        <div class=\"row\">\r\n\r\n\r\n                                            <div class=\"col-md-4\">\r\n                                                <mat-form-field class=\"example-full-width\">\r\n                                                    <input matInput placeholder=\"Departamento\" name=\"deparment\" [(ngModel)]=\"form.deparmetn\" required>\r\n\r\n                                                </mat-form-field>\r\n                                            </div>\r\n                                            <div class=\"col-md-4\">\r\n                                                <mat-form-field class=\"example-full-width\">\r\n                                                    <input matInput placeholder=\"Ciudad\" name=\"city\" [(ngModel)]=\"form.city\" required>\r\n\r\n                                                </mat-form-field>\r\n                                            </div>\r\n                                            <div class=\"col-md-4\">\r\n                                                <mat-form-field class=\"example-full-width\">\r\n                                                    <input matInput placeholder=\"Barrio\" name=\"neighborhood\" [(ngModel)]=\"form.neighborhood\" required>\r\n\r\n                                                </mat-form-field>\r\n                                            </div>\r\n                                        </div>\r\n                                        <hr>\r\n\r\n                                        <div class=\"row\">\r\n\r\n                                            <div class=\"col-md-4\">\r\n                                                <mat-form-field class=\"example-full-width\">\r\n                                                    <input matInput placeholder=\"Estrato\" type=\"number\" name=\"socialStratum\" [(ngModel)]=\"form.socialStratum\" required>\r\n\r\n                                                </mat-form-field>\r\n                                            </div>\r\n                                            <div class=\"col-md-4\">\r\n                                                <mat-form-field class=\"example-full-width\">\r\n                                                    <input matInput placeholder=\"Timpo en vivienda actual\" name=\"timeLiving\" [(ngModel)]=\"form.timeliving\" required>\r\n\r\n                                                </mat-form-field>\r\n                                            </div>\r\n\r\n                                        </div>\r\n                                    </div>\r\n                                    <hr>\r\n                                </div>\r\n                            </div>\r\n\r\n                            <div class=\"card\">\r\n                                <br>\r\n\r\n                                <div class=\"card-body\">\r\n                                    <h4 class=\"txtB text-center\"><b>Información Economica</b></h4>\r\n\r\n                                    <hr>\r\n                                    <div class=\"row\">\r\n                                        <div class=\"col-md-4\">\r\n                                            <mat-form-field class=\"example-full-width\">\r\n                                                <input matInput placeholder=\"Actividad Económica\" name=\"typeActivity\" [(ngModel)]=\"form.typeActivity\" required>\r\n\r\n                                            </mat-form-field>\r\n                                        </div>\r\n                                        <div class=\"col-md-4\">\r\n                                            <mat-form-field class=\"example-full-width\">\r\n                                                <input matInput placeholder=\"Tipo de actividad\" name=\"typeContract\" [(ngModel)]=\"form.typeContract\" required>\r\n\r\n                                            </mat-form-field>\r\n                                        </div>\r\n                                        <div class=\"col-md-4\">\r\n                                            <mat-form-field class=\"example-full-width\">\r\n                                                <input matInput placeholder=\"Nombre empresa\" name=\"company\" [(ngModel)]=\"form.company\" required>\r\n\r\n                                            </mat-form-field>\r\n                                        </div>\r\n                                    </div>\r\n                                    <hr>\r\n                                    <div class=\"row\">\r\n                                        <div class=\"col-md-4\">\r\n                                            <mat-form-field class=\"example-full-width\">\r\n                                                <input matInput placeholder=\"Departamento\" name=\"deparmentCompany\" [(ngModel)]=\"form.deparmentCompany\" required>\r\n\r\n                                            </mat-form-field>\r\n                                        </div>\r\n                                        <div class=\"col-md-4\">\r\n                                            <mat-form-field class=\"example-full-width\">\r\n                                                <input matInput placeholder=\"Ciudad\" name=\"cityCompany\" [(ngModel)]=\"form.cityCompany\" required>\r\n\r\n                                            </mat-form-field>\r\n                                        </div>\r\n                                        <div class=\"col-md-4\">\r\n                                            <mat-form-field class=\"example-full-width\">\r\n                                                <input matInput placeholder=\"Tiempo en trabajo actual\" name=\"timeCompany\" [(ngModel)]=\"form.timeCompany\" required>\r\n\r\n                                            </mat-form-field>\r\n                                        </div>\r\n                                    </div>\r\n                                    <hr>\r\n                                    <div class=\"row\">\r\n                                        <div class=\"col-md-4\">\r\n                                            <mat-form-field class=\"example-full-width\">\r\n                                                <input matInput placeholder=\"Cargo\" name=\"position\" [(ngModel)]=\"form.position\" required>\r\n\r\n                                            </mat-form-field>\r\n                                        </div>\r\n                                        <div class=\"col-md-4\">\r\n                                            <mat-form-field>\r\n                                                <mat-select placeholder=\"Eps\" name=\"eps\" [(ngModel)]=\"form.eps\">\r\n                                                    <mat-option>\r\n                                                        eps1\r\n                                                    </mat-option>\r\n                                                    <mat-option>\r\n                                                        eps2\r\n                                                    </mat-option>\r\n                                                    <mat-option>\r\n                                                        eps3\r\n                                                    </mat-option>\r\n                                                </mat-select>\r\n                                            </mat-form-field>\r\n                                        </div>\r\n                                        <div class=\"col-md-4\">\r\n                                            <mat-form-field>\r\n                                                <mat-select placeholder=\"Fondo de pensión\" name=\"pension\" [(ngModel)]=\"form.pension\">\r\n                                                    <mat-option>\r\n                                                        Pension 1\r\n                                                    </mat-option>\r\n                                                    <mat-option>\r\n                                                        Pension 2\r\n                                                    </mat-option>\r\n                                                    <mat-option>\r\n                                                        Pension 3\r\n                                                    </mat-option>\r\n                                                </mat-select>\r\n                                            </mat-form-field>\r\n                                        </div>\r\n\r\n                                    </div>\r\n                                    <hr>\r\n                                    <div class=\"row\">\r\n                                        <div class=\"col-md-4\">\r\n                                            <mat-form-field class=\"example-full-width\">\r\n                                                <input matInput placeholder=\"Fecha de vinculación\" name=\"datePension\" type=\"date\" [(ngModel)]=\"form.datePension\" required>\r\n\r\n                                            </mat-form-field>\r\n                                        </div>\r\n                                        <div class=\"col-md-4\">\r\n                                            <mat-form-field class=\"example-full-width\">\r\n                                                <input matInput placeholder=\"Dirección de empresa\" name=\"adressCompany\" [(ngModel)]=\"form.adressCompany\" required>\r\n\r\n                                            </mat-form-field>\r\n                                        </div>\r\n                                        <div class=\"col-md-4\">\r\n                                            <mat-form-field class=\"example-full-width\">\r\n                                                <input matInput placeholder=\"Teléfono empresa\" name=\"phoneCompany\" [(ngModel)]=\"form.phoneCompany\" required>\r\n\r\n                                            </mat-form-field>\r\n                                        </div>\r\n\r\n                                    </div>\r\n                                    <hr>\r\n                                    <div class=\"row\">\r\n\r\n                                        <div class=\"col-md-4\">\r\n                                            <mat-form-field class=\"example-full-width\">\r\n                                                <input matInput placeholder=\"Salario básico\" name=\"salary\" [(ngModel)]=\"form.salary\" required>\r\n\r\n                                            </mat-form-field>\r\n                                        </div>\r\n                                        <div class=\"col-md-4\">\r\n                                            <mat-form-field class=\"example-full-width\">\r\n                                                <input matInput placeholder=\"Honorarios\" name=\"fee\" [(ngModel)]=\"form.fee\" required>\r\n\r\n                                            </mat-form-field>\r\n                                        </div>\r\n\r\n                                        <div class=\"col-md-4\">\r\n                                            <mat-form-field class=\"example-full-width\">\r\n                                                <input matInput placeholder=\"Comisiones\" name=\"comissions\" [(ngModel)]=\"form.commissions\" required>\r\n\r\n                                            </mat-form-field>\r\n                                        </div>\r\n                                    </div>\r\n                                    <hr>\r\n\r\n                                    <div class=\"row\">\r\n\r\n                                        <div class=\"col-md-4\">\r\n                                            <mat-form-field class=\"example-full-width\">\r\n                                                <input matInput placeholder=\"Otros ingresos\" name=\"otherSalary\" [(ngModel)]=\"form.otherSalary\" required>\r\n\r\n                                            </mat-form-field>\r\n                                        </div>\r\n                                        <div class=\"col-md-4\">\r\n                                            <mat-form-field class=\"example-full-width\">\r\n                                                <input matInput placeholder=\"Total ingresos\" name=\"totalSalary\" [(ngModel)]=\"form.totalSalary\" required>\r\n\r\n                                            </mat-form-field>\r\n                                        </div>\r\n\r\n                                        <div class=\"col-md-4\">\r\n                                            <mat-form-field class=\"example-full-width\">\r\n                                                <input matInput placeholder=\"Gastos familiaries\" name=\"expeneses\" [(ngModel)]=\"form.expeneses\" required>\r\n\r\n                                            </mat-form-field>\r\n                                        </div>\r\n                                    </div>\r\n                                    <hr>\r\n                                    <div class=\"row\">\r\n                                        <div class=\"col-md-4\">\r\n                                            <mat-form-field class=\"example-full-width\">\r\n                                                <input matInput placeholder=\"Total egresos\" name=\"totalExpenses\" [(ngModel)]=\"form.totalExpenses\" required>\r\n\r\n                                            </mat-form-field>\r\n                                        </div>\r\n                                    </div>\r\n                                    <hr>\r\n\r\n                                </div>\r\n\r\n                            </div>\r\n\r\n                            <div class=\"card\">\r\n                                <div class=\"card-body\">\r\n                                    <br>\r\n                                    <h4 class=\"txtB text-center\"><b>Datos pacientes</b></h4>\r\n                                    <hr>\r\n                                    <div class=\"row\">\r\n                                        <div class=\"col-md-4\">\r\n                                            <mat-form-field class=\"example-full-width\">\r\n                                                <input matInput placeholder=\"Paciente\" name=\"Paciente\" required>\r\n    \r\n                                            </mat-form-field>\r\n    \r\n    \r\n                                        </div>\r\n                                        <div class=\"col-md-4\">\r\n                                            \r\n                                            <mat-form-field>\r\n                                                <mat-select placeholder=\"Tipos de documento\" name=\"documento\" >\r\n                                                    <mat-option>\r\n                                                        Cédula\r\n                                                    </mat-option>\r\n                                                    <mat-option>\r\n                                                        Pasaporte\r\n                                                    </mat-option>\r\n                                                    <mat-option>\r\n                                                        Tarjeta de identidad\r\n                                                    </mat-option>\r\n                                                </mat-select>\r\n                                            </mat-form-field>\r\n                                            \r\n                                            \r\n    \r\n                                        </div>\r\n                                        <div class=\"col-md-4\">\r\n                                            <mat-form-field class=\"example-full-width\">\r\n                                                <input matInput placeholder=\"# documento\" name=\"number\" required>\r\n    \r\n                                            </mat-form-field>\r\n    \r\n    \r\n                                        </div>\r\n\r\n\r\n\r\n                                    </div>\r\n                                    <div class=\"row\">\r\n                                        <div class=\"col-md-4\">\r\n                                            <mat-form-field>\r\n                                                <mat-select placeholder=\"Duración tratamiento\" name=\"documento\" >\r\n                                                    <mat-option>\r\n                                                        años\r\n                                                    </mat-option>\r\n                                                    <mat-option>\r\n                                                       meses\r\n                                                    </mat-option>\r\n                                                    \r\n                                                </mat-select>\r\n                                            </mat-form-field>\r\n                                        </div>\r\n\r\n                                        <div class=\"col-md-4\">\r\n                                            <div class=\"togglebutton fadeIn\">\r\n                                                <label>\r\n                                                   <b> Ortodoncia</b>\r\n                                                      <input type=\"checkbox\" checked=\"\"  name=\"activo\" >\r\n                                                      <span class=\"toggle fadeIn\"></span>\r\n                                                     \r\n                                                    </label>\r\n                                            </div>\r\n                                        </div>\r\n                                        \r\n                                    </div>\r\n                                    <br>\r\n                                    <div class=\"row\">\r\n                                        <div class=\"col-md-4\">\r\n                                            &nbsp;\r\n                                        </div>\r\n                                        <div class=\"col-md-4\">\r\n                                            &nbsp;\r\n                                        </div>\r\n                                        \r\n                                        <div class=\"col-md-4 text-right\">\r\n                                            <button class=\"btn btn-muted\">Agregar Paciente</button>\r\n                                        </div>\r\n                                    </div>\r\n                                                                    \r\n        \r\n\r\n\r\n\r\n                                </div>\r\n                            </div>\r\n                            <br>\r\n                            <div class=\"card\">\r\n                                <div class=\"card-body\">\r\n                                    <br>\r\n                                    <h4 class=\"txtB text-center\"><b>Información adicional</b></h4>\r\n                                    <hr>\r\n                                    <div class=\"row\">\r\n                                        <div class=\"col-md-4\">\r\n                                            <mat-form-field class=\"example-full-width\">\r\n                                                <input matInput placeholder=\"Valor solicitado\" name=\"valueRequested\" [(ngModel)]=\"form.valueRequested\" required>\r\n\r\n                                            </mat-form-field>\r\n\r\n\r\n                                        </div>\r\n                                        <div class=\"col-md-4\">\r\n                                            <mat-form-field class=\"example-full-width\">\r\n                                                <input matInput placeholder=\"Establecimiento\" name=\"place\" [(ngModel)]=\"form.place\" required>\r\n\r\n                                            </mat-form-field>\r\n                                        </div>\r\n                                        <div class=\"col-md-4\">\r\n                                            <mat-form-field class=\"example-full-width\">\r\n                                                <input matInput placeholder=\"Plazo\" name=\"limit\" [(ngModel)]=\"form.limit\" required>\r\n\r\n                                            </mat-form-field>\r\n                                        </div>\r\n\r\n                                        <!-- <div class=\"col-md-3\">\r\n                                        \r\n                                    </div> -->\r\n\r\n                                    </div>\r\n                                    <hr>\r\n                                    <div class=\"row\">\r\n                                        <div class=\"col-md-4\">\r\n                                            <mat-form-field class=\"example-full-width\">\r\n                                                <input matInput placeholder=\"Vendedor\" name=\"seller\" [(ngModel)]=\"form.seller\" required>\r\n\r\n                                            </mat-form-field>\r\n                                        </div>\r\n                                    </div>\r\n                                    <hr>\r\n                                    <div class=\"row\">\r\n                                        <div class=\"col-sm-3\">\r\n                                            <button class=\"btn btn-muted\" type=\"submit\">Guardar</button>\r\n                                        </div>\r\n                                    </div>\r\n\r\n                                </div>\r\n                            </div>\r\n\r\n                            <br>\r\n\r\n\r\n\r\n\r\n                            <br>\r\n                            <br>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n                        </div>\r\n\r\n                    </form>\r\n\r\n\r\n                </div>\r\n            </div>\r\n\r\n\r\n\r\n\r\n        </div>\r\n    </div>\r\n\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/home/home.component.html":
/*!********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/home/home.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <div class=\"wrapper wrapper-full-page\">\r\n    <div class=\"page-header login-page header-filter\" filter-color=\"black\" style=\"background-image: url('./assets/img/bg-pricing.jpg'); background-size: cover; background-position: top center;\">\r\n        \r\n        <div class=\"container\">\r\n            <div class=\"row\">\r\n                <div class=\"col-lg-4 col-md-6 col-sm-6 ml-auto mr-auto\">\r\n                    <form class=\"form\" method=\"\" action=\"\" (ngSubmit)=\"validate()\" #form=\"ngForm\">\r\n                        <div class=\"card card-login card-hidden\">\r\n                            <div class=\"card-header card-header-info text-center home\">\r\n                                <h4 class=\"card-title\">Log in</h4>\r\n\r\n                            </div>\r\n                            <div class=\"card-body \">\r\n                                <p class=\"card-description text-center\">Alivio</p>\r\n                                <span class=\"bmd-form-group\">\r\n                  <div class=\"input-group\">\r\n                    <div class=\"input-group-prepend\">\r\n                      <span class=\"input-group-text\">\r\n                        <i class=\"material-icons\">face</i>\r\n                      </span>\r\n                            </div>\r\n                            <input type=\"text\" class=\"form-control sel\" name=\"user\" [(ngModel)]=\"user\" placeholder=\"Usuario\">\r\n                        </div>\r\n                        </span>\r\n                        <span class=\"bmd-form-group\">\r\n                 \r\n            </span>\r\n                        <span class=\"bmd-form-group\">\r\n                  <div class=\"input-group\">\r\n                    <div class=\"input-group-prepend\">\r\n                      <span class=\"input-group-text\">\r\n                        <i class=\"material-icons\">lock_outline</i>\r\n                      </span>\r\n                </div>\r\n                <input type=\"password\" class=\"form-control sel\" name=\"pasw\" [(ngModel)]=\"pasw\" placeholder=\"Contraseña...\">\r\n            </div>\r\n            </span>\r\n        </div>\r\n        <div class=\"card-footer justify-content-center\">\r\n            <button class=\"btn btn-rose btn-link btn-lg\" type=\"submit\">Ingresar</button>\r\n\r\n        </div>\r\n    </div>\r\n    </form>\r\n</div>\r\n</div>\r\n</div>\r\n<footer class=\"footer \">\r\n    <div class=\"container\">\r\n\r\n        <div class=\"copyright pull-right\">\r\n            &copy; {{test | date: 'yyyy'}}, hecho con <i class=\"material-icons\">favorite</i> por\r\n            <a href=\"http://b612.cloud\" target=\"_blank\">B612 Technologies </a>.\r\n        </div>\r\n    </div>\r\n</footer>\r\n</div>\r\n</div> -->\r\n<div class=\"sidenav\">\r\n    <div class=\"login-main-text\">\r\n        <br>\r\n        <br>\r\n        <img src=\"../../assets/img/logo-alivio-1.png\" class=\"imgL\">\r\n\r\n    </div>\r\n</div>\r\n<div class=\"main\">\r\n\r\n\r\n    <div class=\"col-md-6 col-sm-12\">\r\n\r\n        <div class=\"login-form\">\r\n            <div class=\"card-header fontB\">\r\n                <h3 class=\"card-title colorW\">Bienvenido a Alivio</h3>\r\n            </div>\r\n            <br>\r\n            <br>\r\n            <div class=\"card-body\">\r\n                <form class=\"form\" method=\"\" action=\"\" (ngSubmit)=\"validate()\" #form=\"ngForm\">\r\n                    <span class=\"bmd-form-group\">\r\n                        <div class=\"input-group\">\r\n                          <div class=\"input-group-prepend\">\r\n                            <span class=\"input-group-text\">\r\n                              <i class=\"material-icons colorW\" >face</i>\r\n                            </span>\r\n            </div>\r\n            <input type=\"text\" class=\"form-control sel colorW\" name=\"user\" [(ngModel)]=\"user\" placeholder=\"Usuario\">\r\n        </div>\r\n\r\n        </span>\r\n        <br>\r\n        <span class=\"bmd-form-group\">\r\n                    </span>\r\n        <span class=\"bmd-form-group\">\r\n                        <div class=\"input-group\">\r\n                            <div class=\"input-group-prepend\">\r\n                                <span class=\"input-group-text\">\r\n                                    <i class=\"material-icons colorW\" >lock_outline</i>\r\n                                </span>\r\n    </div>\r\n    <input type=\"password\" class=\"form-control sel colorW\" name=\"pasw\" [(ngModel)]=\"pasw\" placeholder=\"Contraseña...\">\r\n</div>\r\n</span>\r\n<br>\r\n<div class=\"row\">\r\n    <div class=\"col-md-3\">\r\n        <p> </p>\r\n    </div>\r\n    <div class=\"col-md-3\">\r\n        <p> </p>\r\n        <p>&nbsp;&nbsp;&nbsp; </p>\r\n    </div>\r\n    <div class=\"col-sm-2\">\r\n        <p></p>\r\n    </div>\r\n\r\n    <div class=\"col-md-3\">\r\n        <button type=\"submit\" class=\"btn btn-sm btn-black text-right\">Ingresar</button>\r\n    </div>\r\n\r\n</div>\r\n\r\n</form>\r\n\r\n\r\n</div>\r\n\r\n</div>\r\n</div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layouts/admin/admin-layout.component.html":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layouts/admin/admin-layout.component.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<div class=\"wrapper\">\r\n    <div class=\"sidebar\" data-color=\"white\" data-background-color=\"red\" data-image=\"./assets/img/sidebar-1.jpg\">\r\n        <app-sidebar-cmp></app-sidebar-cmp>\r\n        <div class=\"sidebar-background\" style=\"background-image: url(assets/img/sidebar-1.jpg)\"></div>\r\n    </div>\r\n    <div class=\"main-panel\">\r\n        <app-navbar-cmp></app-navbar-cmp>\r\n        <router-outlet></router-outlet>\r\n        <div *ngIf=\"!isMap()\">\r\n            <app-footer-cmp></app-footer-cmp>\r\n        </div>\r\n    </div>\r\n    <app-fixedplugin></app-fixedplugin>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layouts/auth/auth-layout.component.html":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layouts/auth/auth-layout.component.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-expand-lg bg-primary navbar-transparent navbar-absolute\" color-on-scroll=\"500\">\r\n  <div class=\"container\">\r\n    <div class=\"navbar-wrapper\">\r\n      <a class=\"navbar-brand d-none d-sm-none d-md-block\" [routerLink]=\"['/dashboard']\">Material Dashboard Pro Angular</a>\r\n      <a class=\"navbar-brand d-block d-sm-block d-md-none\" [routerLink]=\"['/dashboard']\">MD Pro Angular</a>\r\n    </div>\r\n    <button mat-button class=\"navbar-toggler\" type=\"button\" (click)=\"sidebarToggle()\">\r\n      <span class=\"sr-only\">Toggle navigation</span>\r\n      <span class=\"navbar-toggler-icon icon-bar\"></span>\r\n      <span class=\"navbar-toggler-icon icon-bar\"></span>\r\n      <span class=\"navbar-toggler-icon icon-bar\"></span>\r\n    </button>\r\n    <div class=\"collapse navbar-collapse justify-content-end\">\r\n      <ul class=\"navbar-nav\">\r\n        <li class=\"nav-item\" routerLinkActive=\"active\">\r\n          <a class=\"nav-link\" [routerLink]=\"['/dashboard']\">\r\n            <i class=\"material-icons\">dashboard</i> Dashboard\r\n          </a>\r\n        </li>\r\n        <li class=\"nav-item\" routerLinkActive=\"active\">\r\n          <a class=\"nav-link\" [routerLink]=\"['/pages/register']\">\r\n            <i class=\"material-icons\">person_add</i> Register\r\n          </a>\r\n        </li>\r\n        <li class=\"nav-item\" routerLinkActive=\"active\">\r\n          <a class=\"nav-link\" [routerLink]=\"['/pages/login']\">\r\n            <i class=\"material-icons\">fingerprint</i> Login\r\n          </a>\r\n        </li>\r\n        <li class=\"nav-item\" routerLinkActive=\"active\">\r\n          <a class=\"nav-link\" [routerLink]=\"['/pages/lock']\">\r\n            <i class=\"material-icons\">lock_open</i> Lock\r\n          </a>\r\n        </li>\r\n      </ul>\r\n    </div>\r\n  </div>\r\n</nav>\r\n  <router-outlet></router-outlet>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/login/login.component.html":
/*!**********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/login/login.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"wrapper wrapper-full-page\">\r\n    <div class=\"page-header login-page header-filter\" filter-color=\"black\" style=\"background-image: url('./assets/img/login.jpg'); background-size: cover; background-position: top center;\">\r\n        <!--   you can change the color of the filter page using: data-color=\"blue | purple | green | orange | red | rose \" -->\r\n        <div class=\"container\">\r\n            <div class=\"row\">\r\n                <div class=\"col-lg-4 col-md-6 col-sm-6 ml-auto mr-auto\">\r\n                    <form class=\"form\" method=\"\" action=\"\">\r\n                        <div class=\"card card-login card-hidden\">\r\n                            <div class=\"card-header card-header-rose text-center\">\r\n                                <h4 class=\"card-title\">Log in</h4>\r\n                                <div class=\"social-line\">\r\n                                    <a href=\"#pablo\" class=\"btn btn-just-icon btn-link btn-white\">\r\n                                        <i class=\"fa fa-facebook-square\"></i>\r\n                                    </a>\r\n                                    <a href=\"#pablo\" class=\"btn btn-just-icon btn-link btn-white\">\r\n                                        <i class=\"fa fa-twitter\"></i>\r\n                                    </a>\r\n                                    <a href=\"#pablo\" class=\"btn btn-just-icon btn-link btn-white\">\r\n                                        <i class=\"fa fa-google-plus\"></i>\r\n                                    </a>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"card-body \">\r\n                                <p class=\"card-description text-center\">Or Be Classical</p>\r\n                                <span class=\"bmd-form-group\">\r\n                  <div class=\"input-group\">\r\n                    <div class=\"input-group-prepend\">\r\n                      <span class=\"input-group-text\">\r\n                        <i class=\"material-icons\">face</i>\r\n                      </span>\r\n                            </div>\r\n                            <input type=\"text\" class=\"form-control\" placeholder=\"First Name...\">\r\n                        </div>\r\n                        </span>\r\n                        <span class=\"bmd-form-group\">\r\n                  <div class=\"input-group\">\r\n                    <div class=\"input-group-prepend\">\r\n                      <span class=\"input-group-text\">\r\n                        <i class=\"material-icons\">email</i>\r\n                      </span>\r\n                </div>\r\n                <input type=\"email\" class=\"form-control\" placeholder=\"Email...\">\r\n            </div>\r\n            </span>\r\n            <span class=\"bmd-form-group\">\r\n                  <div class=\"input-group\">\r\n                    <div class=\"input-group-prepend\">\r\n                      <span class=\"input-group-text\">\r\n                        <i class=\"material-icons\">lock_outline</i>\r\n                      </span>\r\n        </div>\r\n        <input type=\"password\" class=\"form-control\" placeholder=\"Password...\">\r\n    </div>\r\n    </span>\r\n</div>\r\n<div class=\"card-footer justify-content-center\">\r\n    <a href=\"#pablo\" class=\"btn btn-rose btn-link btn-lg\">Lets Go</a>\r\n</div>\r\n</div>\r\n</form>\r\n</div>\r\n</div>\r\n</div>\r\n<footer class=\"footer \">\r\n    <div class=\"container\">\r\n        <nav class=\"pull-left\">\r\n            <ul>\r\n                <li>\r\n                    <a href=\"https://www.creative-tim.com\">\r\n                Creative Tim\r\n              </a>\r\n                </li>\r\n                <li>\r\n                    <a href=\"https://creative-tim.com/about-us\">\r\n                About Us\r\n              </a>\r\n                </li>\r\n                <li>\r\n                    <a href=\"http://blog.creative-tim.com\">\r\n                Blog\r\n              </a>\r\n                </li>\r\n                <li>\r\n                    <a href=\"https://www.creative-tim.com/license\">\r\n                Licenses\r\n              </a>\r\n                </li>\r\n            </ul>\r\n        </nav>\r\n        <div class=\"copyright pull-right\">\r\n\r\n        </div>\r\n    </div>\r\n</footer>\r\n</div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/md/md-chart/md-chart.component.html":
/*!*******************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/md/md-chart/md-chart.component.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card\">\r\n  <div class=\"header\">\r\n    <h4 class=\"title\">{{ title }}</h4>\r\n    <p class=\"category\">{{ subtitle }}</p>\r\n  </div>\r\n  <div class=\"content\">\r\n    <div [attr.id]=\"chartId\" class=\"ct-chart {{ chartClass }}\"></div>\r\n\r\n    <div class=\"footer\">\r\n      <div class=\"legend\">\r\n        <span *ngFor=\"let item of legendItems\">\r\n          <i [ngClass]=\"item.imageClass\"></i> {{ item.title }}\r\n        </span>\r\n      </div>\r\n      <hr *ngIf=\"withHr\">\r\n      <div class=\"stats\">\r\n        <i [ngClass]=\"footerIconClass\"></i> {{ footerText }}\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/md/md-table/md-table.component.html":
/*!*******************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/md/md-table/md-table.component.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n  <div class=\"content table-responsive\">\r\n    <table class=\"table\">\r\n      <tbody>\r\n          <tr *ngFor=\"let row of data.dataRows\">\r\n            <!-- <td *ngFor=\"let cell of row\">{{ cell }}</td> -->\r\n            <td>\r\n                <div class=\"flag\">\r\n                    <img src=\"./assets/img/flags/{{row[0]}}.png\" alt=\"\">\r\n                </div>\r\n            </td>\r\n            <td>\r\n                {{row[1]}}\r\n            </td>\r\n            <td class=\"text-right\">\r\n                {{row[2]}}\r\n            </td>\r\n            <td class=\"text-right\">\r\n                {{row[3]}}\r\n            </td>\r\n          </tr>\r\n      </tbody>\r\n    </table>\r\n\r\n  </div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/order/order.component.html":
/*!**********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/order/order.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"sidebar\" data-color=\"danger\" data-background-color=\"white\">\r\n    <app-sidebar-cmp></app-sidebar-cmp>\r\n    <div class=\"sidebar-background\"></div>\r\n</div>\r\n<div class=\"main-panel\">\r\n    <app-navbar-cmp></app-navbar-cmp>\r\n    <div class=\"main-content\">\r\n        <div class=\"container-fluid\">\r\n            <div class=\"row\">\r\n                <div class=\"col-md-11\">\r\n                    <div class=\"card\">\r\n                        <div class=\"card-header card-header-muted card-header-icon\">\r\n                            <div class=\"card-icon\">\r\n                                <i class=\"material-icons\">clear_all</i>\r\n                            </div>\r\n                            <h4 class=\"card-title\">Ordenamiento</h4>\r\n\r\n                        </div>\r\n                        <div class=\"card-body\">\r\n                            <form (ngSubmit)=\"save()\" #form=\"ngForm\">\r\n                                <br>\r\n                                <div class=\"row\">\r\n                                    <div class=\"col-md-3\">\r\n                                        <mat-form-field>\r\n                                            <mat-select placeholder=\"Entidad\" [(ngModel)]=\"first.entity\" name=\"entity\">\r\n                                                <mat-option *ngFor=\"let i of enitys\" [value]=\"i\">\r\n                                                    {{i}}\r\n                                                </mat-option>\r\n\r\n                                            </mat-select>\r\n                                        </mat-form-field>\r\n\r\n\r\n                                    </div>\r\n                                    <div class=\"col-md-3\">\r\n                                        <mat-form-field class=\"example-full-width\">\r\n                                            <input matInput placeholder=\"Cupos\" name=\"quotas\" [(ngModel)]=\"first.quotas\" type=\"number\" required>\r\n\r\n                                        </mat-form-field>\r\n                                    </div>\r\n                                    <div class=\"col-md-3\">\r\n                                        <mat-form-field class=\"example-full-width\">\r\n                                            <input matInput placeholder=\"Costos\" name=\"cost\" [(ngModel)]=\"first.cost\" type=\"number\" required>\r\n\r\n                                        </mat-form-field>\r\n                                    </div>\r\n                                    <div class=\"col-md-3\">\r\n                                        <mat-form-field class=\"example-full-width\">\r\n                                            <input matInput placeholder=\"Puesto\" type=\"number\" [(ngModel)]=\"first.place\" max=\"5\" min=\"1\" name=\"place\" required>\r\n\r\n                                        </mat-form-field>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"row\">\r\n                                    <div class=\"col-md-3\">\r\n                                        <mat-form-field>\r\n                                            <mat-select placeholder=\"Entidad\" name=\"entity2\" [(ngModel)]=\"second.entity\">\r\n                                                <mat-option *ngFor=\"let i of enitys\" [value]=\"i\">\r\n                                                    {{i}}\r\n                                                </mat-option>\r\n\r\n                                            </mat-select>\r\n                                        </mat-form-field>\r\n\r\n\r\n                                    </div>\r\n                                    <div class=\"col-md-3\">\r\n                                        <mat-form-field class=\"example-full-width\">\r\n                                            <input matInput placeholder=\"Cupos\" name=\"quotas2\" [(ngModel)]=\"second.quotas\" type=\"number\" required>\r\n\r\n                                        </mat-form-field>\r\n                                    </div>\r\n                                    <div class=\"col-md-3\">\r\n                                        <mat-form-field class=\"example-full-width\">\r\n                                            <input matInput placeholder=\"Costos\" name=\"cost2\" [(ngModel)]=\"second.cost\" type=\"number\" required>\r\n\r\n                                        </mat-form-field>\r\n                                    </div>\r\n                                    <div class=\"col-md-3\">\r\n                                        <mat-form-field class=\"example-full-width\">\r\n                                            <input matInput placeholder=\"Puesto\" type=\"number\" [(ngModel)]=\"second.place\" max=\"5\" min=\"1\" name=\"place2\" required>\r\n\r\n                                        </mat-form-field>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"row\">\r\n                                    <div class=\"col-md-3\">\r\n                                        <mat-form-field>\r\n                                            <mat-select placeholder=\"Entidad\" name=\"enity3\" [(ngModel)]=\"third.entity\">\r\n                                                <mat-option *ngFor=\"let i of enitys\" [value]=\"i\">\r\n                                                    {{i}}\r\n                                                </mat-option>>\r\n\r\n                                            </mat-select>\r\n                                        </mat-form-field>\r\n\r\n\r\n                                    </div>\r\n                                    <div class=\"col-md-3\">\r\n                                        <mat-form-field class=\"example-full-width\">\r\n                                            <input matInput placeholder=\"Cupos\" name=\"quotas3\" [(ngModel)]=\"third.quotas\" type=\"number\" required>\r\n\r\n                                        </mat-form-field>\r\n                                    </div>\r\n                                    <div class=\"col-md-3\">\r\n                                        <mat-form-field class=\"example-full-width\">\r\n                                            <input matInput placeholder=\"Costos\" name=\"cost3\" [(ngModel)]=\"third.cost\" type=\"number\" required>\r\n\r\n                                        </mat-form-field>\r\n                                    </div>\r\n                                    <div class=\"col-md-3\">\r\n                                        <mat-form-field class=\"example-full-width\">\r\n                                            <input matInput placeholder=\"Puesto\" type=\"number\" [(ngModel)]=\"third.place\" max=\"5\" min=\"1\" name=\"place3\" required>\r\n\r\n                                        </mat-form-field>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"row\">\r\n                                    <div class=\"col-md-3\">\r\n                                        <mat-form-field>\r\n                                            <mat-select placeholder=\"Entidad\" [(ngModel)]=\"fourth.entity\" name=\"type\">\r\n                                                <mat-option *ngFor=\"let i of enitys\" [value]=\"i\">\r\n                                                    {{i}}\r\n                                                </mat-option>>\r\n\r\n                                            </mat-select>\r\n                                        </mat-form-field>\r\n\r\n\r\n                                    </div>\r\n                                    <div class=\"col-md-3\">\r\n                                        <mat-form-field class=\"example-full-width\">\r\n                                            <input matInput placeholder=\"Cupos\" name=\"quotas4\" [(ngModel)]=\"fourth.quotas\" type=\"number\" required>\r\n\r\n                                        </mat-form-field>\r\n                                    </div>\r\n                                    <div class=\"col-md-3\">\r\n                                        <mat-form-field class=\"example-full-width\">\r\n                                            <input matInput placeholder=\"Costos\" name=\"cost4\" [(ngModel)]=\"fourth.cost\" type=\"number\" required>\r\n\r\n                                        </mat-form-field>\r\n                                    </div>\r\n                                    <div class=\"col-md-3\">\r\n                                        <mat-form-field class=\"example-full-width\">\r\n                                            <input matInput placeholder=\"Puesto\" type=\"number\" [(ngModel)]=\"fourth.place\" max=\"5\" min=\"1\" name=\"place4\" required>\r\n\r\n                                        </mat-form-field>\r\n                                    </div>\r\n                                </div>\r\n\r\n\r\n\r\n                                <div class=\"card-footer\">\r\n                                    <div class=\"row\">\r\n                                        <button type=\"submit\" class=\"btn  btn-muted \" [disabled]=\"!form.valid\">Guardar</button>\r\n\r\n\r\n                                    </div>\r\n\r\n                                </div>\r\n\r\n\r\n\r\n\r\n                            </form>\r\n\r\n                        </div>\r\n\r\n                    </div>\r\n\r\n                </div>\r\n\r\n            </div>\r\n\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/reports/reports.component.html":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/reports/reports.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"sidebar\" data-color=\"danger\" data-background-color=\"white\">\r\n    <app-sidebar-cmp></app-sidebar-cmp>\r\n    <div class=\"sidebar-background\"></div>\r\n</div>\r\n<div class=\"main-panel\">\r\n    <app-navbar-cmp></app-navbar-cmp>\r\n    <div class=\"main-content\">\r\n        <div class=\"container-fluid\">\r\n            <div class=\"row\">\r\n                <div class=\"col-md-11\">\r\n                    <div class=\"card\">\r\n                        <div class=\"card-header card-header-muted card-header-icon\">\r\n                            <div class=\"card-icon\">\r\n                                <i class=\"material-icons\">bar_chart</i>\r\n                            </div>\r\n                            <h4 class=\"card-title\">Reportes</h4>\r\n\r\n                        </div>\r\n                        <div class=\"card-body\">\r\n                            <br>\r\n                            <form>\r\n                                <div class=\"row\">\r\n                                    <div class=\"col-md-3\">\r\n                                        <mat-form-field>\r\n                                            <mat-select placeholder=\"Reportes\" name=\"type\">\r\n                                                <mat-option>\r\n                                                    reporte1\r\n                                                </mat-option>\r\n                                                <mat-option>\r\n                                                    reporte2\r\n                                                </mat-option>\r\n                                            </mat-select>\r\n                                        </mat-form-field>\r\n\r\n\r\n                                    </div>\r\n                                    <div class=\"col-md-3\">\r\n                                        <mat-form-field>\r\n                                            <mat-select placeholder=\"Fechas\" name=\"type\">\r\n                                                <mat-option>\r\n                                                    Ultimos 30 días\r\n                                                </mat-option>\r\n                                                <mat-option>\r\n                                                    Hoy\r\n                                                </mat-option>\r\n                                            </mat-select>\r\n                                        </mat-form-field>\r\n\r\n\r\n                                    </div>\r\n                                    <div class=\"col-md-2\">\r\n                                        <button class=\"btn btn-muted\">Generar</button>\r\n                                    </div>\r\n                                </div>\r\n\r\n                            </form>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/request/assignment/assignment.component.html":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/request/assignment/assignment.component.html ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"sidebar\" data-color=\"danger\" data-background-color=\"white\">\n  <app-sidebar-cmp></app-sidebar-cmp>\n  <div class=\"sidebar-background\"></div>\n</div>\n<div class=\"main-panel\">\n\n  <app-navbar-cmp></app-navbar-cmp>\n  <div class=\"main-content\">\n    <div class=\"container-fluid\">\n      <div class=\"row\">\n            <div class=\"col-md-12\">\n              <h4 class=\"card-title\">Asignación Solictudes</h4>\n          </div>\n          \n          <br>\n\n      </div>\n      <div class=\"card\">\n        <div class=\"card-body\">\n          <div class=\"row\">\n            <ul class=\"nav nav-pills nav-pills-stacked-example\" role=\"tablist\">\n              <li class=\"nav-item\">\n                  <a class=\"nav-link active\" data-toggle=\"tab\" href=\"#link1\" role=\"tablist\" >\n                          Básica\n                        </a>\n              </li>\n  \n              <li class=\"nav-item\">\n                  <a class=\"nav-link\" data-toggle=\"tab\" href=\"#link2\" role=\"tablist\" >\n                          Personal\n                        </a>\n              </li>\n              <li class=\"nav-item\">\n                <a class=\"nav-link\" data-toggle=\"tab\" href=\"#link3\" role=\"tablist\" >\n                        Económica\n                      </a>\n            </li>\n              <li class=\"nav-item\">\n                <a class=\"nav-link\" data-toggle=\"tab\" href=\"#link4\" role=\"tablist\" >\n                       Adicional\n                      </a>\n            </li>\n  \n          </ul>\n          </div>\n          <br>\n          <div class=\"tab-content tab-space\" style=\"margin-top: -48px\">\n            <div class=\"material-datatables tab-pane active\" id=\"link1\">\n              <br>\n              \n              <div class=\"row\" style=\"margin-left: 20px;\">\n                <div class=\"col-md-2\">\n                  <b>Información: </b> informacion paciente\n                </div>\n                <div class=\"col-md-2\">\n                  <b>Información: </b> informacion paciente\n                </div>\n                <div class=\"col-md-2\">\n                  <b>Información: </b> informacion paciente\n                </div>\n                <div class=\"col-md-2\">\n                  <b>Información: </b> informacion paciente\n                </div>\n              </div>\n              <br>\n              <div class=\"row\" style=\"margin-left: 20px;\">\n                <div class=\"col-md-2\">\n                  <b>Información: </b> informacion paciente\n                </div>\n                <div class=\"col-md-2\">\n                  <b>Información: </b> informacion paciente\n                </div>\n                <div class=\"col-md-2\">\n                  <b>Información: </b> informacion paciente\n                </div>\n                <div class=\"col-md-2\">\n                  <b>Información: </b> informacion paciente\n                </div>\n              </div>\n            </div>\n\n            <div class=\"material-datatables tab-pane\" id=\"link2\">\n              <br>\n              \n              <div class=\"row\" style=\"margin-left: 20px;\">\n                <div class=\"col-md-2\">\n                  <b>Información: </b> informacion paciente\n                </div>\n                <div class=\"col-md-2\">\n                  <b>Información: </b> informacion paciente\n                </div>\n                <div class=\"col-md-2\">\n                  <b>Información: </b> informacion paciente\n                </div>\n                <div class=\"col-md-2\">\n                  <b>Información: </b> informacion paciente\n                </div>\n              </div>\n              <br>\n              <div class=\"row\" style=\"margin-left: 20px;\">\n                <div class=\"col-md-2\">\n                  <b>Información: </b> informacion paciente\n                </div>\n                <div class=\"col-md-2\">\n                  <b>Información: </b> informacion paciente\n                </div>\n                <div class=\"col-md-2\">\n                  <b>Información: </b> informacion paciente\n                </div>\n                <div class=\"col-md-2\">\n                  <b>Información: </b> informacion paciente\n                </div>\n              </div>\n              <br>\n              <div class=\"row\" style=\"margin-left: 20px;\">\n                <div class=\"col-md-2\">\n                  <b>Información: </b> informacion paciente\n                </div>\n                <div class=\"col-md-2\">\n                  <b>Información: </b> informacion paciente\n                </div>\n                <div class=\"col-md-2\">\n                  <b>Información: </b> informacion paciente\n                </div>\n                <div class=\"col-md-2\">\n                  <b>Información: </b> informacion paciente\n                </div>\n              </div>\n              <br>\n              <div class=\"row\" style=\"margin-left: 20px;\">\n                <div class=\"col-md-2\">\n                  <b>Información: </b> informacion paciente\n                </div>\n                <div class=\"col-md-2\">\n                  <b>Información: </b> informacion paciente\n                </div>\n                <div class=\"col-md-2\">\n                  <b>Información: </b> informacion paciente\n                </div>\n                <div class=\"col-md-2\">\n                  <b>Información: </b> informacion paciente\n                </div>\n              </div>\n            </div>\n\n          </div>\n         \n  \n  \n        </div>\n      </div>\n\n     \n      <div class=\"card\">\n        <div class=\"card-body\">\n            <h4>Bitácora</h4>\n            <hr>\n\n            <div class=\"table\">\n              <table class=\"table\">\n                <thead>\n                  <tr>\n                    <th>Fecha</th>\n                    <th>Modificado por</th>\n                    <th>Financiera</th>\n                    <th>Observación</th>\n                    <th>Adjuntos</th>\n                    <th class=\"text-right\">Ver más</th>\n                  </tr>\n                </thead>\n                <tbody>\n                  <tr>\n                    <td>.....</td>\n                    <td>.....</td>\n                    <td>.....</td>\n                    <td>.....</td>\n                    <td>.....</td>\n                    <td class=\"text-right\"><button class=\"btn btn-muted\" >Ver</button></td>\n                  </tr>\n                  <tr>\n                    <td>.....</td>\n                    <td>.....</td>\n                    <td>.....</td>\n                    <td>.....</td>\n                    <td>.....</td>\n                    <td class=\"text-right\"><button class=\"btn btn-muted\" >Ver</button></td>\n                  </tr>\n                </tbody>\n\n              </table>\n\n            </div>\n        </div>\n      </div>\n\n      <div class=\"card\">\n        <div class=\"card-body\">\n          <h4>Estado solicitud</h4>\n          <hr>\n          <form action=\"\">\n            <div class=\"row\">\n              <div class=\"col-md-4\">\n                <mat-form-field>\n                    <mat-select placeholder=\"Financiera\" name=\"financiera\" >\n                        <mat-option>\n                            f1\n                        </mat-option>\n                        <mat-option>\n                          f1\n                        </mat-option>\n                        <mat-option>\n                          f1\n                        </mat-option>\n                    </mat-select>\n                </mat-form-field>\n            </div>\n  \n            </div>\n            <div class=\"row\">\n                <div class=\"col-md-4\">\n                  <mat-form-field>\n                      <mat-select placeholder=\"Estado solicitud\" name=\"solicitud\" >\n                          <mat-option>\n                              estado1\n                          </mat-option>\n                          <mat-option>\n                            estado2\n                          </mat-option>\n                          <mat-option>\n                            estado2\n                          </mat-option>\n                      </mat-select>\n                  </mat-form-field>\n              </div>\n              <div class=\"col-md-4\">\n                <mat-form-field>\n                    <mat-select placeholder=\"Incidencia\" name=\"Incidencia\" >\n                        <mat-option>\n                          Incidencia1\n                        </mat-option>\n                        <mat-option>\n                          Incidencia1\n                        </mat-option>\n                        <mat-option>\n                          Incidencia1\n                        </mat-option>\n                    </mat-select>\n                </mat-form-field>\n            </div>\n            </div>\n            <br>\n            \n            <div class=\"row\">\n              <div class=\"col-md-4\">\n                <mat-form-field>\n                  <input matInput [matDatepicker]=\"picker\" placeholder=\"Fecha de desembolso\" name=\"fecha\"  (click)=\"picker.open()\" required>\n                  <mat-datepicker-toggle matSuffix [for]=\"picker\"></mat-datepicker-toggle>\n                  <mat-datepicker #picker></mat-datepicker>\n              </mat-form-field>\n              </div>\n              <div class=\"col-md-4\">\n                <mat-form-field>\n                  <input matInput [matDatepicker]=\"picker2\" placeholder=\"Fecha de facturación\" name=\"fefacturacion\"  (click)=\"picker.open()\" required>\n                  <mat-datepicker-toggle matSuffix [for]=\"picker2\"></mat-datepicker-toggle>\n                  <mat-datepicker #picker2></mat-datepicker>\n              </mat-form-field>\n              </div>\n\n            </div>\n            <br>\n            <div class=\"row\">\n              <div class=\"col-md-3\">\n                <mat-form-field class=\"example-full-width\">\n                  <input matInput placeholder=\"Monto desemboloso\" name=\"Monto\"  required>\n\n              </mat-form-field>\n              </div>\n            </div>\n            <br>\n\n            <div class=\"row\">\n              <div class=\"col-md-3\">\n                <mat-form-field>\n                  <mat-select placeholder=\"Cambiar asignación\" name=\"cambiar\" >\n                      <mat-option>\n                        Cambiar\n                      </mat-option>\n                      <mat-option>\n                        Cambiar\n                      </mat-option>\n                      <mat-option>\n                        Cambiar\n                      </mat-option>\n                  </mat-select>\n              </mat-form-field>\n              </div>\n            </div>\n            <br>\n            <div class=\"row\">\n              \n              <div class=\"col-md-4\">\n                <label for=\"exampleFormControlTextarea1\">Observaciones</label>\n                <textarea class=\"form-control\" id=\"exampleFormControlTextarea1\" rows=\"3\"></textarea>\n\n              </div>\n\n\n            </div>\n            <br>\n\n            <div class=\"row\">\n\n              <div class=\"col-md-3\">\n                <button class=\"btn btn-muted\">Guardar</button>\n              </div>\n              <div class=\"col-md-3\">\n                <button class=\"btn btn-muted\">Enviar Comercio</button>\n              </div>\n            </div>\n\n\n            <br>\n          </form>\n\n\n\n        </div>\n    \n\n\n      </div>\n      \n\n\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/request/request.component.html":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/request/request.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"sidebar\" data-color=\"danger\" data-background-color=\"white\">\n  <app-sidebar-cmp></app-sidebar-cmp>\n  <div class=\"sidebar-background\"></div>\n</div>\n<div class=\"main-panel\">\n      <app-navbar-cmp></app-navbar-cmp>\n      <div class=\"main-content\">\n\n        <div class=\"container-fluid\">\n          <div class=\"row\">\n            <div class=\"col-md-12\">\n                <div class=\"card\">\n                    <div class=\"card-header card-header-muted card-header-icon\">\n                        <div class=\"card-icon\">\n                            <i class=\"material-icons\">face</i>\n                        </div>\n                        <div class=\"row\">\n                            <div class=\"col-md-12\">\n                                <h4 class=\"card-title\">Asignación Solictudes</h4>\n                            </div>\n                            \n                            <br>\n                        </div>\n\n                        <div class=\"card-body\">\n                            <div class=\"material-datatables\">\n                                <table id=\"datatables\" class=\"table table-striped table-no-bordered table-hover muted\" cellspacing=\"0\" width=\"100%\" style=\"width:100%\">\n                                    <thead>\n                                        <tr>\n                                            <th>{{dataTable.headerRow[0]}}</th>\n                                            <th>{{dataTable.headerRow[1]}}</th>\n                                            <th>{{dataTable.headerRow[2]}}</th>\n                                            <th>{{dataTable.headerRow[3]}}</th>\n                                            <th>{{dataTable.headerRow[4]}}</th>\n                                            <th>{{dataTable.headerRow[5]}}</th>\n                                            <th>{{dataTable.headerRow[6]}}</th>\n                                            <th>{{dataTable.headerRow[7]}}</th>\n\n                                        </tr>\n                                    </thead>\n                                    <tbody>\n                                        <tr *ngFor=\"let u of dataTable.dataRows | keys \">\n                                            <td class=\"text-left\">{{dataTable.dataRows[u].fecha}}</td>\n                                            <td class=\"text-left\">{{dataTable.dataRows[u].solicitud}}</td>\n                                            <td class=\"text-left\">{{dataTable.dataRows[u].identificacion}}</td>\n                                            <td class=\"text-left\">{{dataTable.dataRows[u].estado}}</td>\n                                            <td class=\"text-left\">{{dataTable.dataRows[u].valorT}}</td>\n                                            <td class=\"text-left\">{{dataTable.dataRows[u].valorS}}</td>\n                                            <td class=\"text-left\">{{dataTable.dataRows[u].financiera}}</td>\n                                            <td class=\"text-right\"><button class=\"btn btn-muted\" [routerLink]=\"['/Request', id]\">Detalles</button></td>\n                                            \n                                           \n                                        </tr>\n\n                                    </tbody>\n                                </table>\n                            </div>\n                        </div>\n\n                    </div>\n                </div>\n            </div>\n        </div>\n\n        </div>\n        \n      </div>\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/shared/fixedplugin/fixedplugin.component.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/shared/fixedplugin/fixedplugin.component.html ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Fixed Plugin configurator, used just for Demo Purpose -->\r\n<div class=\"fixed-plugin\">\r\n    <div class=\"dropdown show-dropdown\">\r\n        <a href=\"#\" data-toggle=\"dropdown\" aria-expanded=\"true\">\r\n            <i class=\"fa fa-cog fa-2x\"> </i>\r\n        </a>\r\n        <ul class=\"dropdown-menu\">\r\n            <li class=\"header-title\"> Sidebar Filters</li>\r\n            <li class=\"adjustments-line\">\r\n                <a href=\"javascript:void(0)\" class=\"switch-trigger active-color\">\r\n                  <div class=\"ml-auto mr-auto\">\r\n                    <span class=\"badge filter badge-purple\" data-color=\"purple\"></span>\r\n                    <span class=\"badge filter badge-azure active\" data-color=\"azure\"></span>\r\n                    <span class=\"badge filter badge-green\" data-color=\"green\"></span>\r\n                    <span class=\"badge filter badge-orange\" data-color=\"orange\"></span>\r\n                    <span class=\"badge filter badge-danger\" data-color=\"danger\"></span>\r\n                    <span class=\"badge filter badge-rose\" data-color=\"rose\"></span>\r\n                  </div>\r\n                    <div class=\"clearfix\"></div>\r\n                </a>\r\n            </li>\r\n            <li class=\"header-title\">Sidebar Background</li>\r\n            <li class=\"adjustments-line\">\r\n                <a href=\"javascript:void(0)\" class=\"switch-trigger background-color\">\r\n                    <div class=\"ml-auto mr-auto\">\r\n                        <span class=\"badge filter badge-white\" data-color=\"white\"></span>\r\n                        <span class=\"badge filter badge-black\" data-color=\"black\"></span>\r\n                        <span class=\"badge filter badge-danger active\" data-color=\"red\"></span>\r\n                    </div>\r\n                    <div class=\"clearfix\"></div>\r\n                </a>\r\n            </li>\r\n            <li class=\"adjustments-line\">\r\n              <a href=\"javascript:void(0)\" class=\"switch-trigger\">\r\n                  <p>Sidebar Mini</p>\r\n                  <label class=\"ml-auto\">\r\n                    <div class=\"togglebutton switch-sidebar-mini\">\r\n                      <label>\r\n                          <input type=\"checkbox\">\r\n                              <span class=\"toggle\"></span>\r\n                      </label>\r\n                    </div>\r\n                  </label>\r\n                  <div class=\"clearfix\"></div>\r\n                  <div class=\"ripple-container\"></div>\r\n              </a>\r\n            </li>\r\n            <li class=\"adjustments-line\">\r\n              <a href=\"javascript:void(0)\" class=\"switch-trigger\">\r\n                  <p>Sidebar Images</p>\r\n                  <label class=\"switch-mini ml-auto\">\r\n                    <div class=\"togglebutton switch-sidebar-image\">\r\n                      <label>\r\n                          <input type=\"checkbox\" checked=\"\">\r\n                            <span class=\"toggle\"></span>\r\n                      </label>\r\n                    </div>\r\n                  </label>\r\n                  <div class=\"clearfix\"></div>\r\n                  <div class=\"ripple-container\"></div>\r\n              </a>\r\n            </li>\r\n            <li class=\"header-title\">Images</li>\r\n            <li class=\"active\">\r\n                <a class=\"img-holder switch-trigger\" href=\"javascript:void(0)\">\r\n                    <img src=\"./assets/img/sidebar-1.jpg\" alt=\"\" />\r\n                </a>\r\n            </li>\r\n            <li>\r\n                <a class=\"img-holder switch-trigger\" href=\"javascript:void(0)\">\r\n                    <img src=\"./assets/img/sidebar-2.jpg\" alt=\"\" />\r\n                </a>\r\n            </li>\r\n            <li>\r\n                <a class=\"img-holder switch-trigger\" href=\"javascript:void(0)\">\r\n                    <img src=\"./assets/img/sidebar-3.jpg\" alt=\"\" />\r\n                </a>\r\n            </li>\r\n            <li>\r\n                <a class=\"img-holder switch-trigger\" href=\"javascript:void(0)\">\r\n                    <img src=\"./assets/img/sidebar-4.jpg\" alt=\"\" />\r\n                </a>\r\n            </li>\r\n        </ul>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/shared/footer/footer.component.html":
/*!*******************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/shared/footer/footer.component.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<footer class=\"footer \">\r\n  <div class=\"container-fluid\">\r\n    <nav class=\"pull-left\">\r\n      <ul>\r\n        <li>\r\n          <a href=\"https://www.creative-tim.com\">\r\n            Creative Tim\r\n          </a>\r\n        </li>\r\n        <li>\r\n          <a href=\"https://creative-tim.com/about-us\">\r\n            About Us\r\n          </a>\r\n        </li>\r\n        <li>\r\n          <a href=\"http://blog.creative-tim.com\">\r\n            Blog\r\n          </a>\r\n        </li>\r\n        <li>\r\n          <a href=\"https://www.creative-tim.com/license\">\r\n            Licenses\r\n          </a>\r\n        </li>\r\n      </ul>\r\n    </nav>\r\n    <div class=\"copyright pull-right\">\r\n      &copy;\r\n      {{test | date: 'yyyy'}}, made with <i class=\"material-icons\">favorite</i> by\r\n      <a href=\"https://www.creative-tim.com\" target=\"_blank\">Creative Tim</a> for a better web.\r\n    </div>\r\n  </div>\r\n</footer>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/shared/navbar/navbar.component.html":
/*!*******************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/shared/navbar/navbar.component.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav #navbar class=\"navbar navbar-expand-lg navbar-transparent  navbar-absolute\">\r\n    <div class=\"container-fluid\">\r\n        <div class=\"navbar-wrapper\">\r\n            <div class=\"navbar-minimize\">\r\n                <button mat-raised-button (click)=\"minimizeSidebar()\" class=\"btn btn-just-icon btn-white btn-fab btn-round\">\r\n          <i class=\"material-icons text_align-center visible-on-sidebar-regular\">more_vert</i>\r\n          <i class=\"material-icons design_bullet-list-67 visible-on-sidebar-mini\">view_list</i>\r\n        </button>\r\n            </div>\r\n            <a class=\"navbar-brand\" href=\"{{getPath()}}\"> {{getTitle()}}</a>\r\n        </div>\r\n        <button mat-button class=\"navbar-toggler btn-no-ripple\" type=\"button\" (click)=\"sidebarToggle()\">\r\n      <span class=\"sr-only\">Toggle navigation</span>\r\n      <span class=\"navbar-toggler-icon icon-bar\"></span>\r\n      <span class=\"navbar-toggler-icon icon-bar\"></span>\r\n      <span class=\"navbar-toggler-icon icon-bar\"></span>\r\n    </button>\r\n\r\n    </div>\r\n</nav>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/sidebar/sidebar.component.html":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/sidebar/sidebar.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"logo\">\r\n    <a class=\"simple-text logo-mini\">\r\n        <div class=\"logo-img\">\r\n            <img src=\"/assets/img/angular2-logo-white.png\" />\r\n        </div>\r\n    </a>\r\n    <a href=\"https://www.creative-tim.com\" class=\"simple-text logo-normal\">\r\n              Alivio\r\n            </a>\r\n</div>\r\n\r\n\r\n<div class=\"sidebar-wrapper\">\r\n\r\n    <div class=\"user\">\r\n        <div class=\"photo\">\r\n            <img src=\"./assets/img/faces/avatar.jpg\" />\r\n        </div>\r\n        <div class=\"user-info\">\r\n            <a data-toggle=\"collapse\" href=\"#collapseExample\" class=\"collapsed\">\r\n                <span>\r\n                            Admin\r\n                            \r\n                        </span>\r\n            </a>\r\n\r\n        </div>\r\n    </div>\r\n    <div *ngIf=\"isMobileMenu()\">\r\n        <form class=\"navbar-form\">\r\n            <span class=\"bmd-form-group\"><div class=\"input-group no-border\">\r\n                  <input type=\"text\" value=\"\" class=\"form-control\" placeholder=\"Search...\">\r\n                  <button mat-raised-button type=\"submit\" class=\"btn btn-white btn-round btn-just-icon\">\r\n                    <i class=\"material-icons\">search</i>\r\n                    <div class=\"ripple-container\"></div>\r\n                  </button>\r\n                </div></span>\r\n        </form>\r\n        <ul class=\"nav navbar-nav nav-mobile-menu\">\r\n            <li class=\"nav-item\">\r\n                <a class=\"nav-link\" href=\"#pablo\">\r\n                    <i class=\"material-icons\">dashboard</i>\r\n                    <p>\r\n                        <span class=\"d-lg-none d-md-block\">Stats</span>\r\n                    </p>\r\n                </a>\r\n            </li>\r\n            <li class=\"nav-item dropdown\">\r\n                <a class=\"nav-link\" href=\"#pablo\" id=\"navbarDropdownMenuLink\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\r\n                    <i class=\"material-icons\">notifications</i>\r\n                    <span class=\"notification\">5</span>\r\n                    <p>\r\n                        <span class=\"d-lg-none d-md-block\">Some Actions</span>\r\n                    </p>\r\n                </a>\r\n\r\n            </li>\r\n            <li class=\"nav-item\">\r\n                <a class=\"nav-link\" href=\"#pablo\">\r\n                    <i class=\"material-icons\">person</i>\r\n                    <p>\r\n                        <span class=\"d-lg-none d-md-block\">Account</span>\r\n                    </p>\r\n                </a>\r\n            </li>\r\n        </ul>\r\n    </div>\r\n    <ul class=\"nav\">\r\n        <li routerLinkActive=\"active\" *ngFor=\"let menuitem of menuItems\" class=\"nav-item\">\r\n            <!--If is a single link-->\r\n            <a [routerLink]=\"[menuitem.path]\" *ngIf=\"menuitem.type === 'link'\" class=\"nav-link\">\r\n                <i class=\"material-icons\">{{menuitem.icontype}}</i>\r\n                <p>{{menuitem.title}}</p>\r\n            </a>\r\n            <!--If it have a submenu-->\r\n            <a data-toggle=\"collapse\" href=\"#{{menuitem.collapse}}\" *ngIf=\"menuitem.type === 'sub'\" (click)=\"updatePS()\" class=\"nav-link\">\r\n                <i class=\"material-icons\">{{menuitem.icontype}}</i>\r\n                <p>{{menuitem.title}}<b class=\"caret\"></b></p>\r\n            </a>\r\n\r\n            <!--Display the submenu items-->\r\n            <div id=\"{{menuitem.collapse}}\" class=\"collapse\" *ngIf=\"menuitem.type === 'sub'\">\r\n                <ul class=\"nav\">\r\n                    <li routerLinkActive=\"active\" *ngFor=\"let childitem of menuitem.children\" class=\"nav-item\">\r\n                        <a [routerLink]=\"[menuitem.path, childitem.path]\" class=\"nav-link\">\r\n                            <span class=\"sidebar-mini\">{{childitem.ab}}</span>\r\n                            <span class=\"sidebar-normal\">{{childitem.title}}</span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </div>\r\n        </li>\r\n\r\n    </ul>\r\n\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/users/user/user.component.html":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/users/user/user.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"sidebar\" data-color=\"danger\" data-background-color=\"white\">\r\n    <app-sidebar-cmp></app-sidebar-cmp>\r\n    <div class=\"sidebar-background\"></div>\r\n</div>\r\n\r\n<div class=\"main-panel\">\r\n    <app-navbar-cmp></app-navbar-cmp>\r\n    <div class=\"main-content\">\r\n        <div class=\"container-fluid\">\r\n            <div class=\"row\">\r\n                <div class=\"col-md-11\">\r\n                    <div class=\"card\">\r\n                        <div class=\"card-header card-header-muted card-header-icon\">\r\n                            <div class=\"card-icon\">\r\n                                <i class=\"material-icons\">face</i>\r\n                            </div>\r\n                            <h4 class=\"card-title\">Usuario</h4>\r\n\r\n                        </div>\r\n\r\n                        <div class=\"card-body\">\r\n                            <form (ngSubmit)=\"save()\" #form=\"ngForm\">\r\n                                <div class=\"togglebutton fadeIn\">\r\n\r\n                                </div>\r\n\r\n                                <h4>Datos</h4>\r\n                                <div class=\"row\">\r\n                                    <div class=\"col-md-3\">\r\n                                        <mat-form-field class=\"example-full-width\">\r\n                                            <input matInput placeholder=\"Nombre\" [(ngModel)]=\"users.name\" name=\"name\" required>\r\n\r\n                                        </mat-form-field>\r\n                                    </div>\r\n                                    <div class=\"col-md-3\">\r\n                                        <mat-form-field class=\"example-full-width\">\r\n                                            <input matInput placeholder=\"Apellido\" [(ngModel)]=\"users.lastName\" name=\"lastName\" required>\r\n\r\n                                        </mat-form-field>\r\n\r\n                                    </div>\r\n                                    <div class=\"col-md-3\">\r\n                                        <mat-form-field class=\"example-full-width\">\r\n                                            <input matInput placeholder=\"Usuario\" [(ngModel)]=\"users.user\" name=\"user\" required>\r\n\r\n                                        </mat-form-field>\r\n                                    </div>\r\n                                </div>\r\n                                <br>\r\n                                <div class=\"row\">\r\n                                    <div class=\"col-md-3\">\r\n\r\n                                        <mat-form-field>\r\n                                            <mat-select placeholder=\"Clínica\" [(ngModel)]=\"users.clinic\" name=\"clinic\">\r\n                                                <mat-option [value]=\"i\" *ngFor=\"let i of clinics\">\r\n                                                    {{i}}\r\n                                                </mat-option>\r\n\r\n\r\n                                            </mat-select>\r\n                                        </mat-form-field>\r\n\r\n                                    </div>\r\n                                    <div class=\"col-md-3\">\r\n                                        <mat-form-field>\r\n                                            <mat-select placeholder=\"Tipo\" [(ngModel)]=\"users.type\" name=\"type\">\r\n                                                <mat-option [value]=\"i\" *ngFor=\"let i of types\">\r\n                                                    {{i}}\r\n                                                </mat-option>\r\n\r\n\r\n                                            </mat-select>\r\n                                        </mat-form-field>\r\n\r\n\r\n                                    </div>\r\n                                    <div claas=\"col-md-3\">\r\n                                        <mat-form-field class=\"example-full-width\">\r\n                                            <input matInput placeholder=\"Correo\" name=\"email\" [(ngModel)]=\"users.email\" required>\r\n\r\n                                        </mat-form-field>\r\n                                    </div>\r\n\r\n\r\n                                </div>\r\n                                <br>\r\n                                <div class=\"row\">\r\n                                    <div class=\"col-md-3\">\r\n                                        <mat-form-field class=\"example-full-width\">\r\n                                            <input matInput placeholder=\"Contraseña\" type=\"password\" [(ngModel)]=\"users.password\" name=\"password\" required>\r\n\r\n                                        </mat-form-field>\r\n                                    </div>\r\n                                    <div class=\"col-md-3\">\r\n                                        <mat-form-field class=\"example-full-width\">\r\n                                            <input matInput placeholder=\"Confirmar Contraseña\" type=\"password\" name=\"password\" required>\r\n\r\n                                        </mat-form-field>\r\n                                    </div>\r\n\r\n                                </div>\r\n\r\n                                <div class=\"card-footer\">\r\n                                    <div class=\"row\">\r\n                                        <button type=\"submit\" class=\"btn  btn-muted \" [disabled]=\"!form.valid\">Guardar</button>\r\n\r\n                                        <button [routerLink]=\"['/Users']\" class=\"btn btn\">Regresar</button>\r\n                                    </div>\r\n\r\n                                </div>\r\n\r\n                            </form>\r\n                        </div>\r\n\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/users/users.component.html":
/*!**********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/users/users.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"sidebar\" data-color=\"danger\" data-background-color=\"white\">\r\n    <app-sidebar-cmp></app-sidebar-cmp>\r\n    <div class=\"sidebar-background\"></div>\r\n</div>\r\n\r\n<div class=\"main-panel\">\r\n    <app-navbar-cmp></app-navbar-cmp>\r\n    <div class=\"main-content\">\r\n        <div class=\"container-fluid\">\r\n            <div class=\"row\">\r\n                <div class=\"col-md-12\">\r\n                    <div class=\"card\">\r\n                        <div class=\"card-header card-header-muted card-header-icon\">\r\n                            <div class=\"card-icon\">\r\n                                <i class=\"material-icons\">face</i>\r\n                            </div>\r\n                            <div class=\"row\">\r\n                                <div class=\"col-md-12\">\r\n                                    <h4 class=\"card-title\">Usuarios</h4>\r\n                                </div>\r\n                                <div class=\"col-md-12 text-right\">\r\n                                    <button [routerLink]=\"['/User','nuevo']\" type=\"button\" class=\"btn btn-muted\">Nuevo Usuario</button>\r\n                                </div>\r\n                                <br>\r\n                            </div>\r\n\r\n                            <div class=\"card-body\">\r\n                                <div class=\"material-datatables\">\r\n                                    <table id=\"datatables\" class=\"table table-striped table-no-bordered table-hover muted\" cellspacing=\"0\" width=\"100%\" style=\"width:100%\">\r\n                                        <thead>\r\n                                            <tr>\r\n                                                <th>{{dataTable.headerRow[0]}}</th>\r\n                                                <th>{{dataTable.headerRow[1]}}</th>\r\n                                                <th>{{dataTable.headerRow[2]}}</th>\r\n                                                <th>{{dataTable.headerRow[3]}}</th>\r\n\r\n                                            </tr>\r\n                                        </thead>\r\n                                        <tbody>\r\n                                            <tr *ngFor=\"let u of users | keys\">\r\n                                                <td>{{users[u].name}}</td>\r\n                                                <td>{{users[u].user}}</td>\r\n                                                <td>\r\n                                                    {{users[u].email}}\r\n                                                </td>\r\n                                                <td>{{users[u].type}} </td>\r\n                                            </tr>\r\n\r\n                                        </tbody>\r\n                                    </table>\r\n                                </div>\r\n                            </div>\r\n\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n        </div>\r\n    </div>\r\n\r\n</div>"

/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppComponent = /** @class */ (function () {
    function AppComponent(router) {
        this.router = router;
    }
    AppComponent.prototype.ngOnInit = function () {
        this._router = this.router.events.filter(function (event) { return event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationEnd"]; }).subscribe(function (event) {
            var body = document.getElementsByTagName('body')[0];
            var modalBackdrop = document.getElementsByClassName('modal-backdrop')[0];
            if (body.classList.contains('modal-open')) {
                body.classList.remove('modal-open');
                modalBackdrop.remove();
            }
        });
    };
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-my-app',
            template: __webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/index.js!./src/app/app.component.html")
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: MaterialModule, AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MaterialModule", function() { return MaterialModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/datepicker */ "./node_modules/@angular/material/esm5/datepicker.es5.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _sidebar_sidebar_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./sidebar/sidebar.module */ "./src/app/sidebar/sidebar.module.ts");
/* harmony import */ var _shared_footer_footer_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./shared/footer/footer.module */ "./src/app/shared/footer/footer.module.ts");
/* harmony import */ var _shared_navbar_navbar_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./shared/navbar/navbar.module */ "./src/app/shared/navbar/navbar.module.ts");
/* harmony import */ var _shared_fixedplugin_fixedplugin_module__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./shared/fixedplugin/fixedplugin.module */ "./src/app/shared/fixedplugin/fixedplugin.module.ts");
/* harmony import */ var _layouts_admin_admin_layout_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./layouts/admin/admin-layout.component */ "./src/app/layouts/admin/admin-layout.component.ts");
/* harmony import */ var _layouts_auth_auth_layout_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./layouts/auth/auth-layout.component */ "./src/app/layouts/auth/auth-layout.component.ts");
/* harmony import */ var _app_routes__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./app.routes */ "./src/app/app.routes.ts");
/* harmony import */ var _login_LoginComponent__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./login/LoginComponent */ "./src/app/login/LoginComponent.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _form_form_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./form/form.component */ "./src/app/form/form.component.ts");
/* harmony import */ var _users_users_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./users/users.component */ "./src/app/users/users.component.ts");
/* harmony import */ var _users_user_user_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./users/user/user.component */ "./src/app/users/user/user.component.ts");
/* harmony import */ var _order_order_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./order/order.component */ "./src/app/order/order.component.ts");
/* harmony import */ var _reports_reports_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./reports/reports.component */ "./src/app/reports/reports.component.ts");
/* harmony import */ var _pipes_keys_pipe__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./pipes/keys.pipe */ "./src/app/pipes/keys.pipe.ts");
/* harmony import */ var _request_request_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./request/request.component */ "./src/app/request/request.component.ts");
/* harmony import */ var _request_assignment_assignment_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./request/assignment/assignment.component */ "./src/app/request/assignment/assignment.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

























var MaterialModule = /** @class */ (function () {
    function MaterialModule() {
    }
    MaterialModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            exports: [],
            declarations: []
        })
    ], MaterialModule);
    return MaterialModule;
}());

var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatAutocompleteModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatButtonToggleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatCheckboxModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatChipsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatStepperModule"],
                _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_6__["MatDatepickerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatDialogModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatExpansionModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatGridListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatMenuModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatNativeDateModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatPaginatorModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatProgressBarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatProgressSpinnerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatRadioModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatRippleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSelectModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSidenavModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSliderModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSlideToggleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSnackBarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSortModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatTabsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatToolbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatTooltipModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_1__["BrowserAnimationsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_http__WEBPACK_IMPORTED_MODULE_2__["HttpModule"],
                MaterialModule,
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatNativeDateModule"],
                _sidebar_sidebar_module__WEBPACK_IMPORTED_MODULE_8__["SidebarModule"],
                _shared_navbar_navbar_module__WEBPACK_IMPORTED_MODULE_10__["NavbarModule"],
                _app_routes__WEBPACK_IMPORTED_MODULE_14__["APP_ROUTING"],
                _shared_footer_footer_module__WEBPACK_IMPORTED_MODULE_9__["FooterModule"],
                _shared_fixedplugin_fixedplugin_module__WEBPACK_IMPORTED_MODULE_11__["FixedpluginModule"]
            ],
            declarations: [
                _login_LoginComponent__WEBPACK_IMPORTED_MODULE_15__["LoginComponent"],
                _app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"],
                _home_home_component__WEBPACK_IMPORTED_MODULE_16__["HomeComponent"],
                _form_form_component__WEBPACK_IMPORTED_MODULE_17__["FormComponent"],
                _users_users_component__WEBPACK_IMPORTED_MODULE_18__["UsersComponent"],
                _users_user_user_component__WEBPACK_IMPORTED_MODULE_19__["UserComponent"],
                _order_order_component__WEBPACK_IMPORTED_MODULE_20__["OrderComponent"],
                _reports_reports_component__WEBPACK_IMPORTED_MODULE_21__["ReportsComponent"],
                _request_request_component__WEBPACK_IMPORTED_MODULE_23__["RequestComponent"],
                _request_assignment_assignment_component__WEBPACK_IMPORTED_MODULE_24__["AssignmentComponent"],
                _pipes_keys_pipe__WEBPACK_IMPORTED_MODULE_22__["KeysPipe"],
                _layouts_admin_admin_layout_component__WEBPACK_IMPORTED_MODULE_12__["AdminLayoutComponent"],
                _layouts_auth_auth_layout_component__WEBPACK_IMPORTED_MODULE_13__["AuthLayoutComponent"]
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/app.routes.ts":
/*!*******************************!*\
  !*** ./src/app/app.routes.ts ***!
  \*******************************/
/*! exports provided: FeatureRoutingModule, APP_ROUTING */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FeatureRoutingModule", function() { return FeatureRoutingModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "APP_ROUTING", function() { return APP_ROUTING; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _form_form_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./form/form.component */ "./src/app/form/form.component.ts");
/* harmony import */ var _users_users_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./users/users.component */ "./src/app/users/users.component.ts");
/* harmony import */ var _users_user_user_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./users/user/user.component */ "./src/app/users/user/user.component.ts");
/* harmony import */ var _order_order_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./order/order.component */ "./src/app/order/order.component.ts");
/* harmony import */ var _reports_reports_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./reports/reports.component */ "./src/app/reports/reports.component.ts");
/* harmony import */ var _request_request_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./request/request.component */ "./src/app/request/request.component.ts");
/* harmony import */ var _request_assignment_assignment_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./request/assignment/assignment.component */ "./src/app/request/assignment/assignment.component.ts");









var routes = [
    { path: 'Home', component: _home_home_component__WEBPACK_IMPORTED_MODULE_1__["HomeComponent"] },
    { path: 'Form', component: _form_form_component__WEBPACK_IMPORTED_MODULE_2__["FormComponent"] },
    { path: 'Users', component: _users_users_component__WEBPACK_IMPORTED_MODULE_3__["UsersComponent"] },
    { path: 'Request', component: _request_request_component__WEBPACK_IMPORTED_MODULE_7__["RequestComponent"] },
    { path: 'Request/:id', component: _request_assignment_assignment_component__WEBPACK_IMPORTED_MODULE_8__["AssignmentComponent"] },
    { path: 'User/:id', component: _users_user_user_component__WEBPACK_IMPORTED_MODULE_4__["UserComponent"] },
    { path: 'Order', component: _order_order_component__WEBPACK_IMPORTED_MODULE_5__["OrderComponent"] },
    { path: 'Reports', component: _reports_reports_component__WEBPACK_IMPORTED_MODULE_6__["ReportsComponent"] },
    { path: '**', pathMatch: 'full', redirectTo: 'Home' }
];
var FeatureRoutingModule = /** @class */ (function () {
    function FeatureRoutingModule() {
    }
    return FeatureRoutingModule;
}());

var APP_ROUTING = _angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"].forRoot(routes);


/***/ }),

/***/ "./src/app/form/form.component.css":
/*!*****************************************!*\
  !*** ./src/app/form/form.component.css ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".sel {\r\n    background-image: linear-gradient(to top, #203669 2px, rgba(156, 39, 176, 0) 2px), linear-gradient(to top, #d2d2d2 1px, rgba(210, 210, 210, 0) 1px)\r\n}\r\n\r\n.txtB {\r\n    color: #203669;\r\n}\r\n\r\n.togglebutton label input[type=checkbox]:checked + .toggle {\r\n    background-color:#203669 !important;\r\n}\r\n\r\n.matfocused{\r\n    background-color:#203669 !important;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZm9ybS9mb3JtLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSTtBQUNKOztBQUVBO0lBQ0ksY0FBYztBQUNsQjs7QUFDQTtJQUNJLG1DQUFtQztBQUN2Qzs7QUFDQTtJQUNJLG1DQUFtQztBQUN2QyIsImZpbGUiOiJzcmMvYXBwL2Zvcm0vZm9ybS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnNlbCB7XHJcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQodG8gdG9wLCAjMjAzNjY5IDJweCwgcmdiYSgxNTYsIDM5LCAxNzYsIDApIDJweCksIGxpbmVhci1ncmFkaWVudCh0byB0b3AsICNkMmQyZDIgMXB4LCByZ2JhKDIxMCwgMjEwLCAyMTAsIDApIDFweClcclxufVxyXG5cclxuLnR4dEIge1xyXG4gICAgY29sb3I6ICMyMDM2Njk7XHJcbn1cclxuLnRvZ2dsZWJ1dHRvbiBsYWJlbCBpbnB1dFt0eXBlPWNoZWNrYm94XTpjaGVja2VkICsgLnRvZ2dsZSB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiMyMDM2NjkgIWltcG9ydGFudDtcclxufVxyXG4ubWF0Zm9jdXNlZHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IzIwMzY2OSAhaW1wb3J0YW50O1xyXG59Il19 */"

/***/ }),

/***/ "./src/app/form/form.component.ts":
/*!****************************************!*\
  !*** ./src/app/form/form.component.ts ***!
  \****************************************/
/*! exports provided: FormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormComponent", function() { return FormComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var jspdf__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! jspdf */ "./node_modules/jspdf/dist/jspdf.min.js");
/* harmony import */ var jspdf__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(jspdf__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var html2canvas__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! html2canvas */ "./node_modules/html2canvas/dist/npm/index.js");
/* harmony import */ var html2canvas__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(html2canvas__WEBPACK_IMPORTED_MODULE_3__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var FormComponent = /** @class */ (function () {
    function FormComponent() {
        this.state = 1;
        this.form = {
            category: '',
            subCategory: '',
            costProduct: 0,
            codeAffiliate: '',
            nameAffiliate: '',
            contactPerson: '',
            phoneContact: '',
            typeRate: '',
            rate: '',
            typeService: '',
            destinationService: '',
            numberPay: 0,
            typeDocument: ' ',
            numberDocument: null,
            firstName: ' ',
            secondName: ' ',
            lastName: ' ',
            secondLastName: ' ',
            date: ' ',
            deparmentBirth: ' ',
            cityBirth: ' ',
            gender: ' ',
            dependents: ' ',
            numberSon: 0,
            typeLiving: ' ',
            state: ' ',
            adress: ' ',
            phoneLiving: ' ',
            expirationDate: 0,
            contryLiving: ' ',
            levelStudies: ' ',
            phone: 0,
            email: ' ',
            deparmetn: ' ',
            city: ' ',
            neighborhood: ' ',
            socialStratum: ' ',
            timeliving: 0,
            typeActivity: ' ',
            typeContract: ' ',
            company: ' ',
            deparmentCompany: ' ',
            cityCompany: ' ',
            timeCompany: ' ',
            position: ' ',
            eps: ' ',
            pension: ' ',
            datePension: ' ',
            adressCompany: ' ',
            phoneCompany: 0,
            salary: 0,
            fee: 0,
            commissions: 0,
            otherSalary: 0,
            totalSalary: 0,
            expeneses: 0,
            totalExpenses: 0,
            valueRequested: 0,
            place: ' ',
            limit: '',
            seller: ' ',
            nameFamily: ' ',
            phoneFamily: ' ',
            similarFamily: ' ',
            deparmentFamily: ' ',
            cityFamily: ' ',
            namePersonal: ' ',
            phonePersonal: ' ',
            deparmentPersonal: ' ',
            cityPersonal: ' ',
        };
    }
    FormComponent.prototype.ngOnInit = function () {
    };
    FormComponent.prototype.changeState = function () {
        if (this.state === 1) {
            this.state = 2;
            return true;
        }
        if (this.state === 2) {
            this.state = 3;
            return true;
        }
        else {
            this.state = 1;
            return true;
        }
    };
    FormComponent.prototype.prevState = function () {
        this.state -= 1;
    };
    FormComponent.prototype.generarPDF = function () {
        html2canvas__WEBPACK_IMPORTED_MODULE_3__(document.getElementById('contenido'), {
            // Opciones
            allowTaint: true,
            useCORS: false,
            // Calidad del PDF
            scale: 1
        }).then(function (canvas) {
            var img = canvas.toDataURL('image/png');
            var doc = new jspdf__WEBPACK_IMPORTED_MODULE_2__();
            doc.addImage(img, 'PNG', 7, 20, 195, 605);
            doc.save('Encuesta.pdf');
        });
    };
    FormComponent.prototype.save = function () {
        if (this.form.numberDocument === 11111) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_1___default()({
                title: "Felicidades su credito fue aprobado",
                text: "",
                buttonsStyling: false,
                confirmButtonClass: "btn btn-success",
                type: "success",
            }).catch(sweetalert2__WEBPACK_IMPORTED_MODULE_1___default.a.noop);
        }
        if (this.form.numberDocument === 22222) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_1___default()({
                title: "Pruebe con otro método de pago",
                text: "",
                buttonsStyling: false,
                confirmButtonClass: "btn btn-warning",
                type: "warning",
            }).catch(sweetalert2__WEBPACK_IMPORTED_MODULE_1___default.a.noop);
        }
    };
    FormComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-form',
            template: __webpack_require__(/*! raw-loader!./form.component.html */ "./node_modules/raw-loader/index.js!./src/app/form/form.component.html"),
            styles: [__webpack_require__(/*! ./form.component.css */ "./src/app/form/form.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], FormComponent);
    return FormComponent;
}());



/***/ }),

/***/ "./src/app/home/home.component.css":
/*!*****************************************!*\
  !*** ./src/app/home/home.component.css ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".home {\r\n    background-color: #fff !important;\r\n    background: #fff !important;\r\n}\r\n\r\n.sel {\r\n    background-image: linear-gradient(to top, #fff 2px, rgba(156, 39, 176, 0) 2px), linear-gradient(to top, #d2d2d2 1px, rgba(210, 210, 210, 0) 1px)\r\n}\r\n\r\nbody {\r\n    font-family: \"Lato\", sans-serif;\r\n}\r\n\r\n.main-head {\r\n    height: 150px;\r\n    background: #fff;\r\n}\r\n\r\n.sidenav {\r\n    height: 100%;\r\n    background-color: #fff;\r\n    overflow-x: hidden;\r\n    padding-top: 20px;\r\n}\r\n\r\n.main {\r\n    background: #203669;\r\n    padding: 0px 10px;\r\n}\r\n\r\n@media screen and (max-height: 450px) {\r\n    .sidenav {\r\n        padding-top: 15px;\r\n    }\r\n}\r\n\r\n@media screen and (max-width: 450px) {\r\n    .login-form {\r\n        margin-top: 10%;\r\n    }\r\n    .register-form {\r\n        margin-top: 10%;\r\n    }\r\n}\r\n\r\n@media screen and (min-width: 768px) {\r\n    .main {\r\n        margin-left: 40%;\r\n    }\r\n    .sidenav {\r\n        width: 40%;\r\n        position: fixed;\r\n        z-index: 1;\r\n        top: 0;\r\n        left: 0;\r\n    }\r\n    .login-form {\r\n        margin-top: 40%;\r\n    }\r\n    .register-form {\r\n        margin-top: 20%;\r\n    }\r\n}\r\n\r\n.login-main-text {\r\n    margin-top: 20%;\r\n    padding: 60px;\r\n    color: #fff;\r\n}\r\n\r\n.login-main-text h2 {\r\n    font-weight: 300;\r\n}\r\n\r\n.btn-black {\r\n    background-color: #fff !important;\r\n    color: #203669;\r\n    font-weight: bold;\r\n}\r\n\r\n.imgL {\r\n    width: 312px;\r\n}\r\n\r\n.colorW {\r\n    color: white;\r\n}\r\n\r\n.fontB {\r\n    background: #203669;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaG9tZS9ob21lLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxpQ0FBaUM7SUFDakMsMkJBQTJCO0FBQy9COztBQUVBO0lBQ0k7QUFDSjs7QUFFQTtJQUNJLCtCQUErQjtBQUNuQzs7QUFFQTtJQUNJLGFBQWE7SUFDYixnQkFBZ0I7QUFDcEI7O0FBRUE7SUFDSSxZQUFZO0lBQ1osc0JBQXNCO0lBQ3RCLGtCQUFrQjtJQUNsQixpQkFBaUI7QUFDckI7O0FBRUE7SUFDSSxtQkFBbUI7SUFDbkIsaUJBQWlCO0FBQ3JCOztBQUVBO0lBQ0k7UUFDSSxpQkFBaUI7SUFDckI7QUFDSjs7QUFFQTtJQUNJO1FBQ0ksZUFBZTtJQUNuQjtJQUNBO1FBQ0ksZUFBZTtJQUNuQjtBQUNKOztBQUVBO0lBQ0k7UUFDSSxnQkFBZ0I7SUFDcEI7SUFDQTtRQUNJLFVBQVU7UUFDVixlQUFlO1FBQ2YsVUFBVTtRQUNWLE1BQU07UUFDTixPQUFPO0lBQ1g7SUFDQTtRQUNJLGVBQWU7SUFDbkI7SUFDQTtRQUNJLGVBQWU7SUFDbkI7QUFDSjs7QUFFQTtJQUNJLGVBQWU7SUFDZixhQUFhO0lBQ2IsV0FBVztBQUNmOztBQUVBO0lBQ0ksZ0JBQWdCO0FBQ3BCOztBQUVBO0lBQ0ksaUNBQWlDO0lBQ2pDLGNBQWM7SUFDZCxpQkFBaUI7QUFDckI7O0FBRUE7SUFDSSxZQUFZO0FBQ2hCOztBQUVBO0lBQ0ksWUFBWTtBQUNoQjs7QUFFQTtJQUNJLG1CQUFtQjtBQUN2QiIsImZpbGUiOiJzcmMvYXBwL2hvbWUvaG9tZS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmhvbWUge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZiAhaW1wb3J0YW50O1xyXG4gICAgYmFja2dyb3VuZDogI2ZmZiAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4uc2VsIHtcclxuICAgIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCh0byB0b3AsICNmZmYgMnB4LCByZ2JhKDE1NiwgMzksIDE3NiwgMCkgMnB4KSwgbGluZWFyLWdyYWRpZW50KHRvIHRvcCwgI2QyZDJkMiAxcHgsIHJnYmEoMjEwLCAyMTAsIDIxMCwgMCkgMXB4KVxyXG59XHJcblxyXG5ib2R5IHtcclxuICAgIGZvbnQtZmFtaWx5OiBcIkxhdG9cIiwgc2Fucy1zZXJpZjtcclxufVxyXG5cclxuLm1haW4taGVhZCB7XHJcbiAgICBoZWlnaHQ6IDE1MHB4O1xyXG4gICAgYmFja2dyb3VuZDogI2ZmZjtcclxufVxyXG5cclxuLnNpZGVuYXYge1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcclxuICAgIG92ZXJmbG93LXg6IGhpZGRlbjtcclxuICAgIHBhZGRpbmctdG9wOiAyMHB4O1xyXG59XHJcblxyXG4ubWFpbiB7XHJcbiAgICBiYWNrZ3JvdW5kOiAjMjAzNjY5O1xyXG4gICAgcGFkZGluZzogMHB4IDEwcHg7XHJcbn1cclxuXHJcbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtaGVpZ2h0OiA0NTBweCkge1xyXG4gICAgLnNpZGVuYXYge1xyXG4gICAgICAgIHBhZGRpbmctdG9wOiAxNXB4O1xyXG4gICAgfVxyXG59XHJcblxyXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA0NTBweCkge1xyXG4gICAgLmxvZ2luLWZvcm0ge1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDEwJTtcclxuICAgIH1cclxuICAgIC5yZWdpc3Rlci1mb3JtIHtcclxuICAgICAgICBtYXJnaW4tdG9wOiAxMCU7XHJcbiAgICB9XHJcbn1cclxuXHJcbkBtZWRpYSBzY3JlZW4gYW5kIChtaW4td2lkdGg6IDc2OHB4KSB7XHJcbiAgICAubWFpbiB7XHJcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDQwJTtcclxuICAgIH1cclxuICAgIC5zaWRlbmF2IHtcclxuICAgICAgICB3aWR0aDogNDAlO1xyXG4gICAgICAgIHBvc2l0aW9uOiBmaXhlZDtcclxuICAgICAgICB6LWluZGV4OiAxO1xyXG4gICAgICAgIHRvcDogMDtcclxuICAgICAgICBsZWZ0OiAwO1xyXG4gICAgfVxyXG4gICAgLmxvZ2luLWZvcm0ge1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDQwJTtcclxuICAgIH1cclxuICAgIC5yZWdpc3Rlci1mb3JtIHtcclxuICAgICAgICBtYXJnaW4tdG9wOiAyMCU7XHJcbiAgICB9XHJcbn1cclxuXHJcbi5sb2dpbi1tYWluLXRleHQge1xyXG4gICAgbWFyZ2luLXRvcDogMjAlO1xyXG4gICAgcGFkZGluZzogNjBweDtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG59XHJcblxyXG4ubG9naW4tbWFpbi10ZXh0IGgyIHtcclxuICAgIGZvbnQtd2VpZ2h0OiAzMDA7XHJcbn1cclxuXHJcbi5idG4tYmxhY2sge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZiAhaW1wb3J0YW50O1xyXG4gICAgY29sb3I6ICMyMDM2Njk7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxufVxyXG5cclxuLmltZ0wge1xyXG4gICAgd2lkdGg6IDMxMnB4O1xyXG59XHJcblxyXG4uY29sb3JXIHtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxufVxyXG5cclxuLmZvbnRCIHtcclxuICAgIGJhY2tncm91bmQ6ICMyMDM2Njk7XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/home/home.component.ts":
/*!****************************************!*\
  !*** ./src/app/home/home.component.ts ***!
  \****************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_2__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var HomeComponent = /** @class */ (function () {
    function HomeComponent(element, router) {
        this.element = element;
        this.router = router;
        this.test = new Date();
        this.user = '';
        this.pasw = '';
        this.nativeElement = element.nativeElement;
        this.sidebarVisible = false;
    }
    HomeComponent.prototype.validate = function () {
        var _this = this;
        console.log(this.user);
        if (this.user === 'admin' && this.pasw === 'admin') {
            console.log('login succes');
            sweetalert2__WEBPACK_IMPORTED_MODULE_2___default()({
                title: "Bienvenido, " + 'admin',
                text: "",
                buttonsStyling: false,
                confirmButtonClass: "btn btn-success",
                type: "success",
                onAfterClose: function () { return _this.router.navigate(['/Form']); }
            }).catch(sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.noop);
        }
    };
    HomeComponent.prototype.ngOnInit = function () {
        var navbar = this.element.nativeElement;
        this.toggleButton = navbar.getElementsByClassName('navbar-toggle')[0];
        var body = document.getElementsByTagName('body')[0];
        body.classList.add('login-page');
        body.classList.add('off-canvas-sidebar');
        var card = document.getElementsByClassName('card')[0];
        setTimeout(function () {
            // after 1000 ms we add the class animated to the login/register card
            card.classList.remove('card-hidden');
        }, 700);
    };
    HomeComponent.prototype.sidebarToggle = function () {
        var toggleButton = this.toggleButton;
        var body = document.getElementsByTagName('body')[0];
        var sidebar = document.getElementsByClassName('navbar-collapse')[0];
        if (this.sidebarVisible == false) {
            setTimeout(function () {
                toggleButton.classList.add('toggled');
            }, 500);
            body.classList.add('nav-open');
            this.sidebarVisible = true;
        }
        else {
            this.toggleButton.classList.remove('toggled');
            this.sidebarVisible = false;
            body.classList.remove('nav-open');
        }
    };
    HomeComponent.prototype.ngOnDestroy = function () {
        var body = document.getElementsByTagName('body')[0];
        body.classList.remove('login-page');
        body.classList.remove('off-canvas-sidebar');
    };
    HomeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! raw-loader!./home.component.html */ "./node_modules/raw-loader/index.js!./src/app/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.css */ "./src/app/home/home.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/intefaces/url.ts":
/*!**********************************!*\
  !*** ./src/app/intefaces/url.ts ***!
  \**********************************/
/*! exports provided: url */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "url", function() { return url; });
//export const url = 'http://localhost:3000/';
var url = 'http://api.alivio.b612.cloud:9090/';


/***/ }),

/***/ "./src/app/layouts/admin/admin-layout.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/layouts/admin/admin-layout.component.ts ***!
  \*********************************************************/
/*! exports provided: AdminLayoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminLayoutComponent", function() { return AdminLayoutComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _md_md_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../md/md.module */ "./src/app/md/md.module.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var rxjs_add_operator_filter__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/add/operator/filter */ "./node_modules/rxjs-compat/_esm5/add/operator/filter.js");
/* harmony import */ var _shared_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../shared/navbar/navbar.component */ "./src/app/shared/navbar/navbar.component.ts");
/* harmony import */ var perfect_scrollbar__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! perfect-scrollbar */ "./node_modules/perfect-scrollbar/dist/perfect-scrollbar.esm.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var AdminLayoutComponent = /** @class */ (function () {
    function AdminLayoutComponent(router, location) {
        this.router = router;
        this.yScrollStack = [];
        this.location = location;
    }
    AdminLayoutComponent.prototype.ngOnInit = function () {
        var _this = this;
        var elemMainPanel = document.querySelector('.main-panel');
        var elemSidebar = document.querySelector('.sidebar .sidebar-wrapper');
        this.location.subscribe(function (ev) {
            _this.lastPoppedUrl = ev.url;
        });
        this.router.events.subscribe(function (event) {
            if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationStart"]) {
                if (event.url != _this.lastPoppedUrl)
                    _this.yScrollStack.push(window.scrollY);
            }
            else if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationEnd"]) {
                if (event.url == _this.lastPoppedUrl) {
                    _this.lastPoppedUrl = undefined;
                    window.scrollTo(0, _this.yScrollStack.pop());
                }
                else
                    window.scrollTo(0, 0);
            }
        });
        this._router = this.router.events.filter(function (event) { return event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationEnd"]; }).subscribe(function (event) {
            elemMainPanel.scrollTop = 0;
            elemSidebar.scrollTop = 0;
        });
        var html = document.getElementsByTagName('html')[0];
        if (window.matchMedia("(min-width: 960px)").matches && !this.isMac()) {
            var ps = new perfect_scrollbar__WEBPACK_IMPORTED_MODULE_6__["default"](elemMainPanel);
            ps = new perfect_scrollbar__WEBPACK_IMPORTED_MODULE_6__["default"](elemSidebar);
            html.classList.add('perfect-scrollbar-on');
        }
        else {
            html.classList.add('perfect-scrollbar-off');
        }
        this._router = this.router.events.filter(function (event) { return event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationEnd"]; }).subscribe(function (event) {
            _this.navbar.sidebarClose();
        });
        this.navItems = [
            { type: _md_md_module__WEBPACK_IMPORTED_MODULE_2__["NavItemType"].NavbarLeft, title: 'Dashboard', iconClass: 'fa fa-dashboard' },
            {
                type: _md_md_module__WEBPACK_IMPORTED_MODULE_2__["NavItemType"].NavbarRight,
                title: '',
                iconClass: 'fa fa-bell-o',
                numNotifications: 5,
                dropdownItems: [
                    { title: 'Notification 1' },
                    { title: 'Notification 2' },
                    { title: 'Notification 3' },
                    { title: 'Notification 4' },
                    { title: 'Another Notification' }
                ]
            },
            {
                type: _md_md_module__WEBPACK_IMPORTED_MODULE_2__["NavItemType"].NavbarRight,
                title: '',
                iconClass: 'fa fa-list',
                dropdownItems: [
                    { iconClass: 'pe-7s-mail', title: 'Messages' },
                    { iconClass: 'pe-7s-help1', title: 'Help Center' },
                    { iconClass: 'pe-7s-tools', title: 'Settings' },
                    'separator',
                    { iconClass: 'pe-7s-lock', title: 'Lock Screen' },
                    { iconClass: 'pe-7s-close-circle', title: 'Log Out' }
                ]
            },
            { type: _md_md_module__WEBPACK_IMPORTED_MODULE_2__["NavItemType"].NavbarLeft, title: 'Search', iconClass: 'fa fa-search' },
            { type: _md_md_module__WEBPACK_IMPORTED_MODULE_2__["NavItemType"].NavbarLeft, title: 'Account' },
            {
                type: _md_md_module__WEBPACK_IMPORTED_MODULE_2__["NavItemType"].NavbarLeft,
                title: 'Dropdown',
                dropdownItems: [
                    { title: 'Action' },
                    { title: 'Another action' },
                    { title: 'Something' },
                    { title: 'Another action' },
                    { title: 'Something' },
                    'separator',
                    { title: 'Separated link' },
                ]
            },
            { type: _md_md_module__WEBPACK_IMPORTED_MODULE_2__["NavItemType"].NavbarLeft, title: 'Log out' }
        ];
    };
    AdminLayoutComponent.prototype.ngAfterViewInit = function () {
        this.runOnRouteChange();
    };
    AdminLayoutComponent.prototype.isMap = function () {
        if (this.location.prepareExternalUrl(this.location.path()) === '/maps/fullscreen') {
            return true;
        }
        else {
            return false;
        }
    };
    AdminLayoutComponent.prototype.runOnRouteChange = function () {
        if (window.matchMedia("(min-width: 960px)").matches && !this.isMac()) {
            var elemSidebar = document.querySelector('.sidebar .sidebar-wrapper');
            var elemMainPanel = document.querySelector('.main-panel');
            var ps = new perfect_scrollbar__WEBPACK_IMPORTED_MODULE_6__["default"](elemMainPanel);
            ps = new perfect_scrollbar__WEBPACK_IMPORTED_MODULE_6__["default"](elemSidebar);
            ps.update();
        }
    };
    AdminLayoutComponent.prototype.isMac = function () {
        var bool = false;
        if (navigator.platform.toUpperCase().indexOf('MAC') >= 0 || navigator.platform.toUpperCase().indexOf('IPAD') >= 0) {
            bool = true;
        }
        return bool;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('sidebar', { static: false }),
        __metadata("design:type", Object)
    ], AdminLayoutComponent.prototype, "sidebar", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_shared_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_5__["NavbarComponent"], { static: false }),
        __metadata("design:type", _shared_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_5__["NavbarComponent"])
    ], AdminLayoutComponent.prototype, "navbar", void 0);
    AdminLayoutComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-layout',
            template: __webpack_require__(/*! raw-loader!./admin-layout.component.html */ "./node_modules/raw-loader/index.js!./src/app/layouts/admin/admin-layout.component.html")
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _angular_common__WEBPACK_IMPORTED_MODULE_3__["Location"]])
    ], AdminLayoutComponent);
    return AdminLayoutComponent;
}());



/***/ }),

/***/ "./src/app/layouts/auth/auth-layout.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/layouts/auth/auth-layout.component.ts ***!
  \*******************************************************/
/*! exports provided: AuthLayoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthLayoutComponent", function() { return AuthLayoutComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AuthLayoutComponent = /** @class */ (function () {
    function AuthLayoutComponent(router, element) {
        this.router = router;
        this.element = element;
        this.mobile_menu_visible = 0;
        this.sidebarVisible = false;
    }
    AuthLayoutComponent.prototype.ngOnInit = function () {
        var _this = this;
        var navbar = this.element.nativeElement;
        this.toggleButton = navbar.getElementsByClassName('navbar-toggler')[0];
        this._router = this.router.events.filter(function (event) { return event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationEnd"]; }).subscribe(function (event) {
            _this.sidebarClose();
            var $layer = document.getElementsByClassName('close-layer')[0];
            if ($layer) {
                $layer.remove();
            }
        });
    };
    AuthLayoutComponent.prototype.sidebarOpen = function () {
        var $toggle = document.getElementsByClassName('navbar-toggler')[0];
        var toggleButton = this.toggleButton;
        var body = document.getElementsByTagName('body')[0];
        setTimeout(function () {
            toggleButton.classList.add('toggled');
        }, 500);
        body.classList.add('nav-open');
        setTimeout(function () {
            $toggle.classList.add('toggled');
        }, 430);
        var $layer = document.createElement('div');
        $layer.setAttribute('class', 'close-layer');
        if (body.querySelectorAll('.wrapper-full-page')) {
            document.getElementsByClassName('wrapper-full-page')[0].appendChild($layer);
        }
        else if (body.classList.contains('off-canvas-sidebar')) {
            document.getElementsByClassName('wrapper-full-page')[0].appendChild($layer);
        }
        setTimeout(function () {
            $layer.classList.add('visible');
        }, 100);
        $layer.onclick = function () {
            body.classList.remove('nav-open');
            this.mobile_menu_visible = 0;
            this.sidebarVisible = false;
            $layer.classList.remove('visible');
            setTimeout(function () {
                $layer.remove();
                $toggle.classList.remove('toggled');
            }, 400);
        }.bind(this);
        body.classList.add('nav-open');
        this.mobile_menu_visible = 1;
        this.sidebarVisible = true;
    };
    ;
    AuthLayoutComponent.prototype.sidebarClose = function () {
        var $toggle = document.getElementsByClassName('navbar-toggler')[0];
        var body = document.getElementsByTagName('body')[0];
        this.toggleButton.classList.remove('toggled');
        var $layer = document.createElement('div');
        $layer.setAttribute('class', 'close-layer');
        this.sidebarVisible = false;
        body.classList.remove('nav-open');
        // $('html').removeClass('nav-open');
        body.classList.remove('nav-open');
        if ($layer) {
            $layer.remove();
        }
        setTimeout(function () {
            $toggle.classList.remove('toggled');
        }, 400);
        this.mobile_menu_visible = 0;
    };
    ;
    AuthLayoutComponent.prototype.sidebarToggle = function () {
        if (this.sidebarVisible === false) {
            this.sidebarOpen();
        }
        else {
            this.sidebarClose();
        }
    };
    AuthLayoutComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-layout',
            template: __webpack_require__(/*! raw-loader!./auth-layout.component.html */ "./node_modules/raw-loader/index.js!./src/app/layouts/auth/auth-layout.component.html")
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]])
    ], AuthLayoutComponent);
    return AuthLayoutComponent;
}());



/***/ }),

/***/ "./src/app/login/LoginComponent.ts":
/*!*****************************************!*\
  !*** ./src/app/login/LoginComponent.ts ***!
  \*****************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var LoginComponent = /** @class */ (function () {
    function LoginComponent() {
    }
    LoginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! raw-loader!./login.component.html */ "./node_modules/raw-loader/index.js!./src/app/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.css */ "./src/app/login/login.component.css")]
        })
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/login/login.component.css":
/*!*******************************************!*\
  !*** ./src/app/login/login.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xvZ2luL2xvZ2luLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/md/md-chart/md-chart.component.css":
/*!****************************************************!*\
  !*** ./src/app/md/md-chart/md-chart.component.css ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21kL21kLWNoYXJ0L21kLWNoYXJ0LmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/md/md-chart/md-chart.component.ts":
/*!***************************************************!*\
  !*** ./src/app/md/md-chart/md-chart.component.ts ***!
  \***************************************************/
/*! exports provided: ChartType, MdChartComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChartType", function() { return ChartType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MdChartComponent", function() { return MdChartComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var chartist__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! chartist */ "./node_modules/chartist/dist/chartist.js");
/* harmony import */ var chartist__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(chartist__WEBPACK_IMPORTED_MODULE_1__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ChartType;
(function (ChartType) {
    ChartType[ChartType["Pie"] = 0] = "Pie";
    ChartType[ChartType["Line"] = 1] = "Line";
    ChartType[ChartType["Bar"] = 2] = "Bar";
})(ChartType || (ChartType = {}));
var MdChartComponent = /** @class */ (function () {
    function MdChartComponent() {
    }
    MdChartComponent_1 = MdChartComponent;
    MdChartComponent.prototype.ngOnInit = function () {
        this.chartId = "md-chart-" + MdChartComponent_1.currentId++;
    };
    MdChartComponent.prototype.ngAfterViewInit = function () {
        switch (this.chartType) {
            case ChartType.Pie:
                new chartist__WEBPACK_IMPORTED_MODULE_1__["Pie"]("#" + this.chartId, this.chartData, this.chartOptions, this.chartResponsive);
                break;
            case ChartType.Line:
                new chartist__WEBPACK_IMPORTED_MODULE_1__["Line"]("#" + this.chartId, this.chartData, this.chartOptions, this.chartResponsive);
                break;
            case ChartType.Bar:
                new chartist__WEBPACK_IMPORTED_MODULE_1__["Bar"]("#" + this.chartId, this.chartData, this.chartOptions, this.chartResponsive);
                break;
        }
    };
    var MdChartComponent_1;
    MdChartComponent.currentId = 1;
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], MdChartComponent.prototype, "title", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], MdChartComponent.prototype, "subtitle", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], MdChartComponent.prototype, "chartClass", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Number)
    ], MdChartComponent.prototype, "chartType", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], MdChartComponent.prototype, "chartData", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], MdChartComponent.prototype, "chartOptions", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Array)
    ], MdChartComponent.prototype, "chartResponsive", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], MdChartComponent.prototype, "footerIconClass", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], MdChartComponent.prototype, "footerText", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Array)
    ], MdChartComponent.prototype, "legendItems", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Boolean)
    ], MdChartComponent.prototype, "withHr", void 0);
    MdChartComponent = MdChartComponent_1 = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-md-chart',
            template: __webpack_require__(/*! raw-loader!./md-chart.component.html */ "./node_modules/raw-loader/index.js!./src/app/md/md-chart/md-chart.component.html"),
            styles: [__webpack_require__(/*! ./md-chart.component.css */ "./src/app/md/md-chart/md-chart.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], MdChartComponent);
    return MdChartComponent;
}());



/***/ }),

/***/ "./src/app/md/md-table/md-table.component.ts":
/*!***************************************************!*\
  !*** ./src/app/md/md-table/md-table.component.ts ***!
  \***************************************************/
/*! exports provided: MdTableComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MdTableComponent", function() { return MdTableComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var MdTableComponent = /** @class */ (function () {
    function MdTableComponent() {
    }
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], MdTableComponent.prototype, "title", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], MdTableComponent.prototype, "subtitle", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], MdTableComponent.prototype, "cardClass", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], MdTableComponent.prototype, "data", void 0);
    MdTableComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-md-table',
            template: __webpack_require__(/*! raw-loader!./md-table.component.html */ "./node_modules/raw-loader/index.js!./src/app/md/md-table/md-table.component.html"),
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectionStrategy"].OnPush
        }),
        __metadata("design:paramtypes", [])
    ], MdTableComponent);
    return MdTableComponent;
}());



/***/ }),

/***/ "./src/app/md/md.module.ts":
/*!*********************************!*\
  !*** ./src/app/md/md.module.ts ***!
  \*********************************/
/*! exports provided: NavItemType, MdModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavItemType", function() { return NavItemType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MdModule", function() { return MdModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _md_table_md_table_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./md-table/md-table.component */ "./src/app/md/md-table/md-table.component.ts");
/* harmony import */ var _md_chart_md_chart_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./md-chart/md-chart.component */ "./src/app/md/md-chart/md-chart.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var NavItemType;
(function (NavItemType) {
    NavItemType[NavItemType["Sidebar"] = 1] = "Sidebar";
    NavItemType[NavItemType["NavbarLeft"] = 2] = "NavbarLeft";
    NavItemType[NavItemType["NavbarRight"] = 3] = "NavbarRight"; // Right-aligned link on navbar in desktop mode, shown above sidebar items on collapsed sidebar in mobile mode
})(NavItemType || (NavItemType = {}));
var MdModule = /** @class */ (function () {
    function MdModule() {
    }
    MdModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]
            ],
            declarations: [
                _md_table_md_table_component__WEBPACK_IMPORTED_MODULE_3__["MdTableComponent"],
                _md_chart_md_chart_component__WEBPACK_IMPORTED_MODULE_4__["MdChartComponent"]
            ],
            exports: [
                _md_table_md_table_component__WEBPACK_IMPORTED_MODULE_3__["MdTableComponent"],
                _md_chart_md_chart_component__WEBPACK_IMPORTED_MODULE_4__["MdChartComponent"]
            ]
        })
    ], MdModule);
    return MdModule;
}());



/***/ }),

/***/ "./src/app/order/order.component.css":
/*!*******************************************!*\
  !*** ./src/app/order/order.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL29yZGVyL29yZGVyLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/order/order.component.ts":
/*!******************************************!*\
  !*** ./src/app/order/order.component.ts ***!
  \******************************************/
/*! exports provided: OrderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderComponent", function() { return OrderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var OrderComponent = /** @class */ (function () {
    function OrderComponent() {
        this.first = {
            entity: 'Entidad 1',
            quotas: 1000,
            cost: 100000,
            place: 1
        };
        this.second = {
            entity: 'Entidad 2',
            quotas: 1000,
            cost: 100000,
            place: 3
        };
        this.third = {
            entity: 'Entidad 3',
            quotas: 1000,
            cost: 100000,
            place: 2
        };
        this.fourth = {
            entity: 'Entidad 4',
            quotas: 1000,
            cost: 100000,
            place: 4
        };
        this.fifth = {
            entity: 'Entidad 5',
            quotas: 1000,
            cost: 100000,
            place: 5
        };
        this.newEntity = {
            first: this.first,
            second: this.second,
            third: this.third,
            fourth: this.fourth,
            fifth: this.fifth
        };
        this.enitys = ['Entidad 1', 'Entidad 2', 'Entidad 3', 'Entidad 4', 'Entidad 5'];
    }
    OrderComponent.prototype.ngOnInit = function () {
    };
    OrderComponent.prototype.save = function () {
        console.log('save');
    };
    OrderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-order',
            template: __webpack_require__(/*! raw-loader!./order.component.html */ "./node_modules/raw-loader/index.js!./src/app/order/order.component.html"),
            styles: [__webpack_require__(/*! ./order.component.css */ "./src/app/order/order.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], OrderComponent);
    return OrderComponent;
}());



/***/ }),

/***/ "./src/app/pipes/keys.pipe.ts":
/*!************************************!*\
  !*** ./src/app/pipes/keys.pipe.ts ***!
  \************************************/
/*! exports provided: KeysPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "KeysPipe", function() { return KeysPipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var KeysPipe = /** @class */ (function () {
    function KeysPipe() {
    }
    KeysPipe.prototype.transform = function (value) {
        var keys = [];
        // tslint:disable-next-line:forin
        for (var key in value) {
            keys.push(key);
        }
        return keys;
    };
    KeysPipe = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"])({
            name: 'keys'
        })
    ], KeysPipe);
    return KeysPipe;
}());



/***/ }),

/***/ "./src/app/reports/reports.component.css":
/*!***********************************************!*\
  !*** ./src/app/reports/reports.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".sidebar li.active>a {\r\n    background-color: #203669 !important;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcmVwb3J0cy9yZXBvcnRzLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxvQ0FBb0M7QUFDeEMiLCJmaWxlIjoic3JjL2FwcC9yZXBvcnRzL3JlcG9ydHMuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5zaWRlYmFyIGxpLmFjdGl2ZT5hIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICMyMDM2NjkgIWltcG9ydGFudDtcclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/reports/reports.component.ts":
/*!**********************************************!*\
  !*** ./src/app/reports/reports.component.ts ***!
  \**********************************************/
/*! exports provided: ReportsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReportsComponent", function() { return ReportsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ReportsComponent = /** @class */ (function () {
    function ReportsComponent() {
    }
    ReportsComponent.prototype.ngOnInit = function () {
    };
    ReportsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-reports',
            template: __webpack_require__(/*! raw-loader!./reports.component.html */ "./node_modules/raw-loader/index.js!./src/app/reports/reports.component.html"),
            styles: [__webpack_require__(/*! ./reports.component.css */ "./src/app/reports/reports.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], ReportsComponent);
    return ReportsComponent;
}());



/***/ }),

/***/ "./src/app/request/assignment/assignment.component.css":
/*!*************************************************************!*\
  !*** ./src/app/request/assignment/assignment.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3JlcXVlc3QvYXNzaWdubWVudC9hc3NpZ25tZW50LmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/request/assignment/assignment.component.ts":
/*!************************************************************!*\
  !*** ./src/app/request/assignment/assignment.component.ts ***!
  \************************************************************/
/*! exports provided: AssignmentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AssignmentComponent", function() { return AssignmentComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AssignmentComponent = /** @class */ (function () {
    function AssignmentComponent() {
    }
    AssignmentComponent.prototype.ngOnInit = function () {
    };
    AssignmentComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-assignment',
            template: __webpack_require__(/*! raw-loader!./assignment.component.html */ "./node_modules/raw-loader/index.js!./src/app/request/assignment/assignment.component.html"),
            styles: [__webpack_require__(/*! ./assignment.component.css */ "./src/app/request/assignment/assignment.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], AssignmentComponent);
    return AssignmentComponent;
}());



/***/ }),

/***/ "./src/app/request/request.component.css":
/*!***********************************************!*\
  !*** ./src/app/request/request.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3JlcXVlc3QvcmVxdWVzdC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/request/request.component.ts":
/*!**********************************************!*\
  !*** ./src/app/request/request.component.ts ***!
  \**********************************************/
/*! exports provided: RequestComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RequestComponent", function() { return RequestComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var RequestComponent = /** @class */ (function () {
    function RequestComponent() {
        this.info1 = {
            fecha: '2/05/20',
            solicitud: 'sol',
            identificacion: '100000',
            estado: 'activo',
            valorT: '2999',
            valorS: '30232',
            financiera: 'Denti salud'
        };
        this.info2 = {
            fecha: '6/05/20',
            solicitud: 'sol',
            identificacion: '340000',
            estado: 'activo',
            valorT: '8989',
            valorS: '30232',
            financiera: 'Refinancia'
        };
        this.dataTable = {
            headerRow: ['Fecha', 'Solicitud', 'Identificación', 'Estado', 'Valor Tratamiento', 'Valor solicitado', 'Financiera', 'Detalles'],
            dataRows: [this.info1, this.info2],
        };
    }
    RequestComponent.prototype.ngOnInit = function () {
    };
    RequestComponent.prototype.ngAfterViewInit = function () {
        $('#datatables').DataTable({
            "pagingType": "full_numbers",
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Buscar asignación",
            },
        });
        var table = $('#datatables').DataTable();
        // Edit record
        table.on('click', '.edit', function (e) {
            var $tr = $(this).closest('tr');
            var data = table.row($tr).data();
            alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
            e.preventDefault();
        });
        // Delete a record
        table.on('click', '.remove', function (e) {
            var $tr = $(this).closest('tr');
            table.row($tr).remove().draw();
            e.preventDefault();
        });
        //Like record
        table.on('click', '.like', function (e) {
            alert('You clicked on Like button');
            e.preventDefault();
        });
        $('.card .material-datatables label').addClass('form-group');
    };
    RequestComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-request',
            template: __webpack_require__(/*! raw-loader!./request.component.html */ "./node_modules/raw-loader/index.js!./src/app/request/request.component.html"),
            styles: [__webpack_require__(/*! ./request.component.css */ "./src/app/request/request.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], RequestComponent);
    return RequestComponent;
}());



/***/ }),

/***/ "./src/app/services/users.service.ts":
/*!*******************************************!*\
  !*** ./src/app/services/users.service.ts ***!
  \*******************************************/
/*! exports provided: UsersService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersService", function() { return UsersService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _intefaces_url__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../intefaces/url */ "./src/app/intefaces/url.ts");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var UsersService = /** @class */ (function () {
    function UsersService(http) {
        this.http = http;
        this.uri = _intefaces_url__WEBPACK_IMPORTED_MODULE_1__["url"] + 'user';
    }
    UsersService.prototype.newUser = function (Users) {
        var body = Users;
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({
            'content-Type': 'application/json',
        });
        return this.http.post(this.uri, body, { headers: headers }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res.json(); }));
    };
    UsersService.prototype.errorHandler = function (error) {
        console.log("estoy en errorHandler");
        console.log(error);
        var er = '';
        var mos = [];
        var mos2 = [];
        var mostrar = '';
        console.log(error.error);
        console.log(JSON.stringify(error));
        er = JSON.stringify(error);
        er.substr(6, 11);
        console.log(er.substr(18, 37));
        console.log(er[0]);
        mos = er.split(',');
        console.log(mos[0]);
        mos2 = mos[0].split('"');
        console.log(mos2);
        console.log(error);
        var type = ['', 'info', 'success', 'warning', 'danger', 'rose', 'primary'];
        var msgError = "";
        var color = 4;
        //  let err = JSON.stringify(error);
        if (error.status === 0) {
            msgError = 'Servicios no disponibles,Consulte al administrador.';
        }
        else {
            msgError = '<b>Error</b>: ' + mos2[3];
        }
        $.notify({
            icon: 'notifications',
            message: msgError
        }, {
            type: type[color],
            timer: 10000,
            placement: {
                from: 'top',
                align: 'right'
            },
            template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0} alert-with-icon" role="alert">' +
                // tslint:disable-next-line:max-line-length
                '<button mat-raised-button type="button" aria-hidden="true" class="close" data-notify="dismiss">  <i class="material-icons">close</i></button>' +
                '<i class="material-icons" data-notify="icon">notifications</i> ' +
                '<span data-notify="title">{1}</span> ' +
                '<span data-notify="message">{2}</span>' +
                '<div class="progress" data-notify="progressbar">' +
                // tslint:disable-next-line:max-line-length
                '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                '</div>' +
                '<a href="{3}" target="{4}" data-notify="url"></a>' +
                '</div>'
        });
        var err = "Ocurrio un error";
        return err;
    };
    UsersService.prototype.getUsers = function () {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({
            'content-Type': 'application/json',
            'x-auth-token': localStorage.getItem("token")
        });
        return this.http.get(this.uri, { headers: headers }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res.json(); }));
    };
    UsersService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"]])
    ], UsersService);
    return UsersService;
}());



/***/ }),

/***/ "./src/app/shared/fixedplugin/fixedplugin.component.css":
/*!**************************************************************!*\
  !*** ./src/app/shared/fixedplugin/fixedplugin.component.css ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9maXhlZHBsdWdpbi9maXhlZHBsdWdpbi5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/shared/fixedplugin/fixedplugin.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/shared/fixedplugin/fixedplugin.component.ts ***!
  \*************************************************************/
/*! exports provided: FixedpluginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FixedpluginComponent", function() { return FixedpluginComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var md = {
    misc: {
        navbar_menu_visible: 0,
        active_collapse: true,
        disabled_collapse_init: 0,
    }
};
var FixedpluginComponent = /** @class */ (function () {
    function FixedpluginComponent() {
    }
    FixedpluginComponent.prototype.ngOnInit = function () {
        // fixed plugin
        var $sidebar = $('.sidebar');
        var $sidebar_img_container = $sidebar.find('.sidebar-background');
        //
        var $full_page = $('.full-page');
        //
        var $sidebar_responsive = $('body > .navbar-collapse');
        var window_width = $(window).width();
        var fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();
        if (window_width > 767 && fixed_plugin_open === 'Dashboard') {
            if ($('.fixed-plugin .dropdown').hasClass('show-dropdown')) {
                $('.fixed-plugin .dropdown').addClass('open');
            }
        }
        $('.fixed-plugin a').click(function (event) {
            // Alex: if we click on switch, stop propagation of the event,
            // so the dropdown will not be hide, otherwise we set the  section active
            if ($(this).hasClass('switch-trigger')) {
                if (event.stopPropagation) {
                    event.stopPropagation();
                }
                else if (window.event) {
                    window.event.cancelBubble = true;
                }
            }
        });
        $('.fixed-plugin .active-color span').click(function () {
            var $full_page_background = $('.full-page-background');
            $(this).siblings().removeClass('active');
            $(this).addClass('active');
            var new_color = $(this).data('color');
            if ($sidebar.length !== 0) {
                $sidebar.attr('data-color', new_color);
            }
            if ($full_page.length !== 0) {
                $full_page.attr('filter-color', new_color);
            }
            if ($sidebar_responsive.length !== 0) {
                $sidebar_responsive.attr('data-color', new_color);
            }
        });
        $('.fixed-plugin .background-color span').click(function () {
            $(this).siblings().removeClass('active');
            $(this).addClass('active');
            var new_color = $(this).data('color');
            if ($sidebar.length !== 0) {
                $sidebar.attr('data-background-color', new_color);
            }
        });
        $('.fixed-plugin .img-holder').click(function () {
            var $full_page_background = $('.full-page-background');
            $(this).parent('li').siblings().removeClass('active');
            $(this).parent('li').addClass('active');
            var new_image = $(this).find('img').attr('src');
            if ($sidebar_img_container.length !== 0 && $('.switch-sidebar-image input:checked').length !== 0) {
                $sidebar_img_container.fadeOut('fast', function () {
                    $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
                    $sidebar_img_container.fadeIn('fast');
                });
            }
            if ($full_page_background.length !== 0 && $('.switch-sidebar-image input:checked').length !== 0) {
                var new_image_full_page_1 = $('.fixed-plugin li.active .img-holder').find('img').data('src');
                $full_page_background.fadeOut('fast', function () {
                    $full_page_background.css('background-image', 'url("' + new_image_full_page_1 + '")');
                    $full_page_background.fadeIn('fast');
                });
            }
            if ($('.switch-sidebar-image input:checked').length === 0) {
                new_image = $('.fixed-plugin li.active .img-holder').find('img').attr('src');
                var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');
                $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
                $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
            }
            if ($sidebar_responsive.length !== 0) {
                $sidebar_responsive.css('background-image', 'url("' + new_image + '")');
            }
        });
        $('.switch-sidebar-image input').change(function () {
            var $full_page_background = $('.full-page-background');
            var $input = $(this);
            if ($input.is(':checked')) {
                if ($sidebar_img_container.length !== 0) {
                    $sidebar_img_container.fadeIn('fast');
                    $sidebar.attr('data-image', '#');
                }
                if ($full_page_background.length !== 0) {
                    $full_page_background.fadeIn('fast');
                    $full_page.attr('data-image', '#');
                }
                var background_image = true;
            }
            else {
                if ($sidebar_img_container.length !== 0) {
                    $sidebar.removeAttr('data-image');
                    $sidebar_img_container.fadeOut('fast');
                }
                if ($full_page_background.length !== 0) {
                    $full_page.removeAttr('data-image', '#');
                    $full_page_background.fadeOut('fast');
                }
                var background_image = false;
            }
        });
        $('.switch-sidebar-mini input').change(function () {
            var $body = $('body');
            var $input = $(this);
            if (md.misc.sidebar_mini_active === true) {
                $('body').removeClass('sidebar-mini');
                md.misc.sidebar_mini_active = false;
            }
            else {
                setTimeout(function () {
                    $('body').addClass('sidebar-mini');
                    $('.sidebar .collapse').css('height', 'auto');
                    md.misc.sidebar_mini_active = true;
                }, 300);
            }
            // we simulate the window Resize so the charts will get updated in realtime.
            var simulateWindowResize = setInterval(function () {
                window.dispatchEvent(new Event('resize'));
            }, 180);
            // we stop the simulation of Window Resize after the animations are completed
            setTimeout(function () {
                clearInterval(simulateWindowResize);
            }, 1000);
        });
    };
    FixedpluginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-fixedplugin',
            template: __webpack_require__(/*! raw-loader!./fixedplugin.component.html */ "./node_modules/raw-loader/index.js!./src/app/shared/fixedplugin/fixedplugin.component.html"),
            styles: [__webpack_require__(/*! ./fixedplugin.component.css */ "./src/app/shared/fixedplugin/fixedplugin.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], FixedpluginComponent);
    return FixedpluginComponent;
}());



/***/ }),

/***/ "./src/app/shared/fixedplugin/fixedplugin.module.ts":
/*!**********************************************************!*\
  !*** ./src/app/shared/fixedplugin/fixedplugin.module.ts ***!
  \**********************************************************/
/*! exports provided: FixedpluginModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FixedpluginModule", function() { return FixedpluginModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _fixedplugin_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./fixedplugin.component */ "./src/app/shared/fixedplugin/fixedplugin.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var FixedpluginModule = /** @class */ (function () {
    function FixedpluginModule() {
    }
    FixedpluginModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"]
            ],
            declarations: [_fixedplugin_component__WEBPACK_IMPORTED_MODULE_2__["FixedpluginComponent"]],
            exports: [_fixedplugin_component__WEBPACK_IMPORTED_MODULE_2__["FixedpluginComponent"]]
        })
    ], FixedpluginModule);
    return FixedpluginModule;
}());



/***/ }),

/***/ "./src/app/shared/footer/footer.component.ts":
/*!***************************************************!*\
  !*** ./src/app/shared/footer/footer.component.ts ***!
  \***************************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var FooterComponent = /** @class */ (function () {
    function FooterComponent() {
        this.test = new Date();
    }
    FooterComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-footer-cmp',
            template: __webpack_require__(/*! raw-loader!./footer.component.html */ "./node_modules/raw-loader/index.js!./src/app/shared/footer/footer.component.html")
        })
    ], FooterComponent);
    return FooterComponent;
}());



/***/ }),

/***/ "./src/app/shared/footer/footer.module.ts":
/*!************************************************!*\
  !*** ./src/app/shared/footer/footer.module.ts ***!
  \************************************************/
/*! exports provided: FooterModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterModule", function() { return FooterModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _footer_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./footer.component */ "./src/app/shared/footer/footer.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var FooterModule = /** @class */ (function () {
    function FooterModule() {
    }
    FooterModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"], _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"]],
            declarations: [_footer_component__WEBPACK_IMPORTED_MODULE_3__["FooterComponent"]],
            exports: [_footer_component__WEBPACK_IMPORTED_MODULE_3__["FooterComponent"]]
        })
    ], FooterModule);
    return FooterModule;
}());



/***/ }),

/***/ "./src/app/shared/navbar/navbar.component.ts":
/*!***************************************************!*\
  !*** ./src/app/shared/navbar/navbar.component.ts ***!
  \***************************************************/
/*! exports provided: NavbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavbarComponent", function() { return NavbarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../.././sidebar/sidebar.component */ "./src/app/sidebar/sidebar.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var misc = {
    navbar_menu_visible: 0,
    active_collapse: true,
    disabled_collapse_init: 0,
};
var NavbarComponent = /** @class */ (function () {
    function NavbarComponent(location, renderer, element, router) {
        this.renderer = renderer;
        this.element = element;
        this.router = router;
        this.mobile_menu_visible = 0;
        this.location = location;
        this.nativeElement = element.nativeElement;
        this.sidebarVisible = false;
    }
    NavbarComponent.prototype.minimizeSidebar = function () {
        var body = document.getElementsByTagName('body')[0];
        if (misc.sidebar_mini_active === true) {
            body.classList.remove('sidebar-mini');
            misc.sidebar_mini_active = false;
        }
        else {
            setTimeout(function () {
                body.classList.add('sidebar-mini');
                misc.sidebar_mini_active = true;
            }, 300);
        }
        // we simulate the window Resize so the charts will get updated in realtime.
        var simulateWindowResize = setInterval(function () {
            window.dispatchEvent(new Event('resize'));
        }, 180);
        // we stop the simulation of Window Resize after the animations are completed
        setTimeout(function () {
            clearInterval(simulateWindowResize);
        }, 1000);
    };
    NavbarComponent.prototype.hideSidebar = function () {
        var body = document.getElementsByTagName('body')[0];
        var sidebar = document.getElementsByClassName('sidebar')[0];
        if (misc.hide_sidebar_active === true) {
            setTimeout(function () {
                body.classList.remove('hide-sidebar');
                misc.hide_sidebar_active = false;
            }, 300);
            setTimeout(function () {
                sidebar.classList.remove('animation');
            }, 600);
            sidebar.classList.add('animation');
        }
        else {
            setTimeout(function () {
                body.classList.add('hide-sidebar');
                // $('.sidebar').addClass('animation');
                misc.hide_sidebar_active = true;
            }, 300);
        }
        // we simulate the window Resize so the charts will get updated in realtime.
        var simulateWindowResize = setInterval(function () {
            window.dispatchEvent(new Event('resize'));
        }, 180);
        // we stop the simulation of Window Resize after the animations are completed
        setTimeout(function () {
            clearInterval(simulateWindowResize);
        }, 1000);
    };
    NavbarComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.listTitles = _sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_1__["ROUTES"].filter(function (listTitle) { return listTitle; });
        var navbar = this.element.nativeElement;
        var body = document.getElementsByTagName('body')[0];
        this.toggleButton = navbar.getElementsByClassName('navbar-toggler')[0];
        if (body.classList.contains('sidebar-mini')) {
            misc.sidebar_mini_active = true;
        }
        if (body.classList.contains('hide-sidebar')) {
            misc.hide_sidebar_active = true;
        }
        this._router = this.router.events.filter(function (event) { return event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_2__["NavigationEnd"]; }).subscribe(function (event) {
            _this.sidebarClose();
            var $layer = document.getElementsByClassName('close-layer')[0];
            if ($layer) {
                $layer.remove();
            }
        });
    };
    NavbarComponent.prototype.onResize = function (event) {
        if ($(window).width() > 991) {
            return false;
        }
        return true;
    };
    NavbarComponent.prototype.sidebarOpen = function () {
        var $toggle = document.getElementsByClassName('navbar-toggler')[0];
        var toggleButton = this.toggleButton;
        var body = document.getElementsByTagName('body')[0];
        setTimeout(function () {
            toggleButton.classList.add('toggled');
        }, 500);
        body.classList.add('nav-open');
        setTimeout(function () {
            $toggle.classList.add('toggled');
        }, 430);
        var $layer = document.createElement('div');
        $layer.setAttribute('class', 'close-layer');
        if (body.querySelectorAll('.main-panel')) {
            document.getElementsByClassName('main-panel')[0].appendChild($layer);
        }
        else if (body.classList.contains('off-canvas-sidebar')) {
            document.getElementsByClassName('wrapper-full-page')[0].appendChild($layer);
        }
        setTimeout(function () {
            $layer.classList.add('visible');
        }, 100);
        $layer.onclick = function () {
            body.classList.remove('nav-open');
            this.mobile_menu_visible = 0;
            this.sidebarVisible = false;
            $layer.classList.remove('visible');
            setTimeout(function () {
                $layer.remove();
                $toggle.classList.remove('toggled');
            }, 400);
        }.bind(this);
        body.classList.add('nav-open');
        this.mobile_menu_visible = 1;
        this.sidebarVisible = true;
    };
    ;
    NavbarComponent.prototype.sidebarClose = function () {
        var $toggle = document.getElementsByClassName('navbar-toggler')[0];
        var body = document.getElementsByTagName('body')[0];
        this.toggleButton.classList.remove('toggled');
        var $layer = document.createElement('div');
        $layer.setAttribute('class', 'close-layer');
        this.sidebarVisible = false;
        body.classList.remove('nav-open');
        // $('html').removeClass('nav-open');
        body.classList.remove('nav-open');
        if ($layer) {
            $layer.remove();
        }
        setTimeout(function () {
            $toggle.classList.remove('toggled');
        }, 400);
        this.mobile_menu_visible = 0;
    };
    ;
    NavbarComponent.prototype.sidebarToggle = function () {
        if (this.sidebarVisible === false) {
            this.sidebarOpen();
        }
        else {
            this.sidebarClose();
        }
    };
    NavbarComponent.prototype.getTitle = function () {
        var titlee = this.location.prepareExternalUrl(this.location.path());
        if (titlee.charAt(0) === '#') {
            titlee = titlee.slice(1);
        }
        for (var i = 0; i < this.listTitles.length; i++) {
            if (this.listTitles[i].type === "link" && this.listTitles[i].path === titlee) {
                return this.listTitles[i].title;
            }
            else if (this.listTitles[i].type === "sub") {
                for (var j = 0; j < this.listTitles[i].children.length; j++) {
                    var subtitle = this.listTitles[i].path + '/' + this.listTitles[i].children[j].path;
                    // console.log(subtitle)
                    // console.log(titlee)
                    if (subtitle === titlee) {
                        return this.listTitles[i].children[j].title;
                    }
                }
            }
        }
        return 'Dashboard';
    };
    NavbarComponent.prototype.getPath = function () {
        return this.location.prepareExternalUrl(this.location.path());
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('app-navbar-cmp', { static: false }),
        __metadata("design:type", Object)
    ], NavbarComponent.prototype, "button", void 0);
    NavbarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-navbar-cmp',
            template: __webpack_require__(/*! raw-loader!./navbar.component.html */ "./node_modules/raw-loader/index.js!./src/app/shared/navbar/navbar.component.html")
        }),
        __metadata("design:paramtypes", [_angular_common__WEBPACK_IMPORTED_MODULE_3__["Location"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], NavbarComponent);
    return NavbarComponent;
}());



/***/ }),

/***/ "./src/app/shared/navbar/navbar.module.ts":
/*!************************************************!*\
  !*** ./src/app/shared/navbar/navbar.module.ts ***!
  \************************************************/
/*! exports provided: NavbarModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavbarModule", function() { return NavbarModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _navbar_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./navbar.component */ "./src/app/shared/navbar/navbar.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var NavbarModule = /** @class */ (function () {
    function NavbarModule() {
    }
    NavbarModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"], _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatButtonModule"]],
            declarations: [_navbar_component__WEBPACK_IMPORTED_MODULE_3__["NavbarComponent"]],
            exports: [_navbar_component__WEBPACK_IMPORTED_MODULE_3__["NavbarComponent"]]
        })
    ], NavbarModule);
    return NavbarModule;
}());



/***/ }),

/***/ "./src/app/sidebar/sidebar.component.ts":
/*!**********************************************!*\
  !*** ./src/app/sidebar/sidebar.component.ts ***!
  \**********************************************/
/*! exports provided: ROUTES, SidebarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ROUTES", function() { return ROUTES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SidebarComponent", function() { return SidebarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var perfect_scrollbar__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! perfect-scrollbar */ "./node_modules/perfect-scrollbar/dist/perfect-scrollbar.esm.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


//Menu Items
var ROUTES = [
    {
        path: '/Form',
        title: 'Solicitud financiera',
        type: 'link',
        icontype: 'content_paste'
    },
    // {
    //     path: '/Users',
    //     title: 'Crear Usuario',
    //     type: 'link',
    //     icontype:'face'
    // },
    {
        path: '/Request',
        title: 'Asignación de solicitudes',
        type: 'link',
        icontype: 'face'
    },
    {
        path: '/Order',
        title: 'Ordenamiento',
        type: 'link',
        icontype: 'clear_all'
    },
    {
        path: '/Reports',
        title: 'Reportes',
        type: 'link',
        icontype: 'bar_chart'
    }
];
var SidebarComponent = /** @class */ (function () {
    function SidebarComponent() {
    }
    SidebarComponent.prototype.isMobileMenu = function () {
        if ($(window).width() > 991) {
            return false;
        }
        return true;
    };
    ;
    SidebarComponent.prototype.ngOnInit = function () {
        this.menuItems = ROUTES.filter(function (menuItem) { return menuItem; });
        if (window.matchMedia("(min-width: 960px)").matches && !this.isMac()) {
            var elemSidebar = document.querySelector('.sidebar .sidebar-wrapper');
            this.ps = new perfect_scrollbar__WEBPACK_IMPORTED_MODULE_1__["default"](elemSidebar);
        }
    };
    SidebarComponent.prototype.updatePS = function () {
        if (window.matchMedia("(min-width: 960px)").matches && !this.isMac()) {
            this.ps.update();
        }
    };
    SidebarComponent.prototype.isMac = function () {
        var bool = false;
        if (navigator.platform.toUpperCase().indexOf('MAC') >= 0 || navigator.platform.toUpperCase().indexOf('IPAD') >= 0) {
            bool = true;
        }
        return bool;
    };
    SidebarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-sidebar-cmp',
            template: __webpack_require__(/*! raw-loader!./sidebar.component.html */ "./node_modules/raw-loader/index.js!./src/app/sidebar/sidebar.component.html"),
        })
    ], SidebarComponent);
    return SidebarComponent;
}());



/***/ }),

/***/ "./src/app/sidebar/sidebar.module.ts":
/*!*******************************************!*\
  !*** ./src/app/sidebar/sidebar.module.ts ***!
  \*******************************************/
/*! exports provided: SidebarModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SidebarModule", function() { return SidebarModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _sidebar_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./sidebar.component */ "./src/app/sidebar/sidebar.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var SidebarModule = /** @class */ (function () {
    function SidebarModule() {
    }
    SidebarModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"], _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"]],
            declarations: [_sidebar_component__WEBPACK_IMPORTED_MODULE_3__["SidebarComponent"]],
            exports: [_sidebar_component__WEBPACK_IMPORTED_MODULE_3__["SidebarComponent"]]
        })
    ], SidebarModule);
    return SidebarModule;
}());



/***/ }),

/***/ "./src/app/users/user/user.component.css":
/*!***********************************************!*\
  !*** ./src/app/users/user/user.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VzZXJzL3VzZXIvdXNlci5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/users/user/user.component.ts":
/*!**********************************************!*\
  !*** ./src/app/users/user/user.component.ts ***!
  \**********************************************/
/*! exports provided: UserComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserComponent", function() { return UserComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_users_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/users.service */ "./src/app/services/users.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_3__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var UserComponent = /** @class */ (function () {
    function UserComponent(userService, router) {
        this.userService = userService;
        this.router = router;
        this.users = {
            name: '',
            lastName: '',
            user: '',
            clinic: '',
            type: '',
            email: '',
            password: '',
        };
        this.types = ['admin', 'user'];
        this.clinics = ['clinca 1', 'clinica 2'];
    }
    UserComponent.prototype.ngOnInit = function () {
    };
    UserComponent.prototype.save = function () {
        var _this = this;
        console.log(this.users);
        this.userService.newUser(this.users).subscribe(function (data) {
            if (typeof data !== 'string') {
                sweetalert2__WEBPACK_IMPORTED_MODULE_3___default()({
                    title: "Guardado",
                    text: "",
                    buttonsStyling: false,
                    confirmButtonClass: "btn btn-success",
                    type: "success",
                    onAfterClose: function () { return _this.router.navigate(['/Users']); }
                }).catch(sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.noop);
            }
        }, function (error) {
            console.log(error);
        });
    };
    UserComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-user',
            template: __webpack_require__(/*! raw-loader!./user.component.html */ "./node_modules/raw-loader/index.js!./src/app/users/user/user.component.html"),
            styles: [__webpack_require__(/*! ./user.component.css */ "./src/app/users/user/user.component.css")]
        }),
        __metadata("design:paramtypes", [_services_users_service__WEBPACK_IMPORTED_MODULE_1__["UsersService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], UserComponent);
    return UserComponent;
}());



/***/ }),

/***/ "./src/app/users/users.component.css":
/*!*******************************************!*\
  !*** ./src/app/users/users.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VzZXJzL3VzZXJzLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/users/users.component.ts":
/*!******************************************!*\
  !*** ./src/app/users/users.component.ts ***!
  \******************************************/
/*! exports provided: UsersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersComponent", function() { return UsersComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_users_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/users.service */ "./src/app/services/users.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var UsersComponent = /** @class */ (function () {
    function UsersComponent(userServices) {
        var _this = this;
        this.userServices = userServices;
        this.users = [];
        this.userServices.getUsers().subscribe(function (data) {
            console.log(data);
            _this.users = data.body;
            console.log(_this.users);
            _this.dataTable = {
                headerRow: ['Nombre', 'Usuario', 'Correo', 'Tipo'],
                dataRows: [_this.users],
            };
        });
    }
    UsersComponent.prototype.ngOnInit = function () {
    };
    UsersComponent.prototype.ngAfterViewInit = function () {
        $('#datatables').DataTable({
            "pagingType": "full_numbers",
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Buscar usuario",
            },
        });
        var table = $('#datatables').DataTable();
        // Edit record
        table.on('click', '.edit', function (e) {
            var $tr = $(this).closest('tr');
            var data = table.row($tr).data();
            alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
            e.preventDefault();
        });
        // Delete a record
        table.on('click', '.remove', function (e) {
            var $tr = $(this).closest('tr');
            table.row($tr).remove().draw();
            e.preventDefault();
        });
        //Like record
        table.on('click', '.like', function (e) {
            alert('You clicked on Like button');
            e.preventDefault();
        });
        $('.card .material-datatables label').addClass('form-group');
    };
    UsersComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-users',
            template: __webpack_require__(/*! raw-loader!./users.component.html */ "./node_modules/raw-loader/index.js!./src/app/users/users.component.html"),
            styles: [__webpack_require__(/*! ./users.component.css */ "./src/app/users/users.component.css")]
        }),
        __metadata("design:paramtypes", [_services_users_service__WEBPACK_IMPORTED_MODULE_1__["UsersService"]])
    ], UsersComponent);
    return UsersComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");
/*!

 =========================================================
 * Material Dashboard PRO Angular - v2.3.0
 =========================================================

 * Product Page: https://www.creative-tim.com/product/material-dashboard-pro-angular2
 * Copyright 2019 Creative Tim (https://www.creative-tim.com)

 * Coded by Creative Tim

 =========================================================

 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 */




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\Usuario\Documents\Angular\aliviofront\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map